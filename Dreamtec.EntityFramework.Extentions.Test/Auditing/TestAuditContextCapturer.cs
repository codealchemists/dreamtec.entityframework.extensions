﻿using System;
using System.Runtime.CompilerServices;
using Dreamtec.EntityFramework.Extensions.Auditing;
using System.Collections.Generic;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing
{
	public class TestAuditContextCapturer : IAuditContextCapturer
	{
		public ChangeCaptureDbContext DbContext { get; set; }

		public Guid LoggedInId { get; set; }

		private bool _alreadyCaptured = false;

		public TestAuditContextCapturer(ChangeCaptureDbContext DbContext)
		{
			this.DbContext = DbContext;
		}

		public IAuditContextCapturer Audit(object triggerActionOwningObject,
			[CallerMemberName]
			string triggerActionName = null)
		{
			if (!_alreadyCaptured)
			{

				AuditContextState auditContextState = DbContext.AuditContextState;

				if (auditContextState == null)
				{
					auditContextState = DbContext.AuditContextState = new AuditContextState();
				}

				auditContextState.DateCaptured = DateTimeOffset.Now;
				auditContextState.LoginId = LoggedInId;
				auditContextState.TriggerActionOwningObjectName = triggerActionOwningObject == null ? null : triggerActionOwningObject.GetType().Name;
				auditContextState.TriggerActionName = triggerActionName;
				auditContextState.UserIpAddress = "1.1.1.1";
				auditContextState.Username = "tester";
				auditContextState.EndpointName = "test";
				_alreadyCaptured = true;
			}

			return this;
		}

		public void SetUsername(string username)
		{
			DbContext.AuditContextState.Username = username;
		}

		public IAuditContextCapturer Journal(IJournalableEntity actionOwningObject, Verbosity verbosity, Confidentiality confidentiality, string userComment = null, string systemComment = null, IJournalableEntity[] associatedEntities = null, [CallerMemberName] string actionName = null)
		{
			AuditContextState auditContextState = DbContext.AuditContextState;

			if (auditContextState == null)
			{
				auditContextState = DbContext.AuditContextState = new AuditContextState();
			}

			JournalEntry journalEntry = new JournalEntry();
			journalEntry.Verbosity = verbosity;
			journalEntry.Confidentiality = confidentiality;
			journalEntry.UserComment = userComment;
			journalEntry.SystemComment = systemComment;
			journalEntry.ActionOwningObjectName = actionOwningObject == null ? null : actionOwningObject.GetType().Name;
			journalEntry.ActionName = actionName;
			journalEntry.AssociatedEntities = new List<IJournalableEntity>();

			if (actionOwningObject != null && typeof(IJournalableEntity).IsAssignableFrom(actionOwningObject.GetType()))
			{
				journalEntry.AssociatedEntities.Add((IJournalableEntity)actionOwningObject);
			}

			if (associatedEntities != null) {
				((List<IJournalableEntity>)journalEntry.AssociatedEntities).AddRange(associatedEntities);
			}

			auditContextState.JournalEntries.Add(journalEntry);

			return this;
		}
	}
}
