using System.Data.Entity.ModelConfiguration;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing.Model.Mapping
{
	internal class PersonMap : EntityTypeConfiguration<Person>
    {
        public PersonMap()
        {
            this.HasKey(t => t.Id);

            this.Property(t => t.Id)
                .IsRequired();

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Surname)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Age)
                .IsRequired();

            this.Property(t => t.BirthdayDate);

            this.ToTable("Person", "audittest");

            this.Ignore(t => t.SomeRuntimeVar);
        }
    }
}
