using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Dreamtec.EntityFramework.Extensions.Auditing.Model.Mapping
{
	internal class EntityClassTypeMap : EntityTypeConfiguration<EntityClassType>
	{
		public EntityClassTypeMap()
		{
			this.HasKey(t => t.Id);

			this.Property(t => t.Id)
				.IsRequired();

			this.Property(t => t.FullTypeName)
				.IsRequired()
				.HasMaxLength(250);

			this.Property(t => t.IsStale)
				.IsRequired();

			this.Property(t => t.IsDeleted)
				.IsRequired();

			this.ToTable("EntityClassType", "dbo");
		}
	}
}
