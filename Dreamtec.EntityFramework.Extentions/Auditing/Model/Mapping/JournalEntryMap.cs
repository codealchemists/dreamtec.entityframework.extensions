using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Dreamtec.EntityFramework.Extensions.Auditing.Model.Mapping
{
	internal class JournalEntryMap : EntityTypeConfiguration<JournalEntry>
    {
        public JournalEntryMap()
        {
			this.HasKey(t => t.Id);

			this.Property(t => t.Id)
				.IsRequired();

			this.Property(t => t.DateApplied)
			   .IsRequired();

			this.Property(t => t.TransactionId)
				.IsRequired();

			this.Property(t => t.UserComment)
				.HasMaxLength(8000);

			this.Property(t => t.SystemComment)
				.HasMaxLength(8000);

			this.Property(t => t.Verbosity)
				.IsRequired();

			this.Property(t => t.Confidentiality)
				.IsRequired();

			this.Property(t => t.ActionOwningObjectName)
				.HasMaxLength(250);

			this.Property(t => t.ActionName)
				.HasMaxLength(250);

			this.ToTable("JournalEntry", "audit");

			this.HasRequired(t => t.Transaction)
			.WithMany(t => t.JournalEntries)
			.HasForeignKey(d => d.TransactionId);
		}
    }
}
