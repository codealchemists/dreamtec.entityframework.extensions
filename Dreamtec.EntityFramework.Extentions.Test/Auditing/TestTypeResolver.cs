﻿using Dreamtec.EntityFramework.Extensions.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing
{
    public class TestTypeResolver : ITypeResolver
    {
        public Type _returnType;

        public TestTypeResolver(Type returnType)
        {
            _returnType = returnType;
        }

        public Type Resolve(Type type)
        {
            return _returnType;
        }
    }
}
