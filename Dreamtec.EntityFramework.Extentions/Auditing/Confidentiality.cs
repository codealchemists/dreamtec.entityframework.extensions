﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
    /// <summary>
    /// The confidentiality / privilege level of the <see cref="JournalEntry"/>
    /// </summary>
    public enum Confidentiality : byte
    {
        Confidential = 0,
        Public = 1
    }
}
