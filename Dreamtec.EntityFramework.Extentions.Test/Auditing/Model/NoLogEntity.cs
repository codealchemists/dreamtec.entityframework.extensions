using System;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing.Model
{
    public class NoLogEntity
    {
        public virtual Guid Id { get; set; }
    }
}
