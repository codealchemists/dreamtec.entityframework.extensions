﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
	public class BeforeSaveChangesEventArgs : EventArgs
	{
		public DbContext DbContext { get; }
		internal BeforeSaveChangesEventArgs(DbContext dbContext)
		{
			DbContext = dbContext ;
		}
	}

}



