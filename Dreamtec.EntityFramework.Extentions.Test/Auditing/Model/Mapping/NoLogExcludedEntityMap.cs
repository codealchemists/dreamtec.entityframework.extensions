using System.Data.Entity.ModelConfiguration;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing.Model.Mapping
{
	internal class NoLogExcludedEntityMap : EntityTypeConfiguration<NoLogExcludedEntity>
    {
        public NoLogExcludedEntityMap()
        {

            this.Property(t => t.Id);

            this.ToTable("NoLogExcludedEntity", "audittest");
        }
    }
}
