using System.Data.Entity.ModelConfiguration;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing.Model.Mapping
{
	internal class NoLogEntityMap : EntityTypeConfiguration<NoLogEntity>
    {
        public NoLogEntityMap()
        {

            this.Property(t => t.Id);

            this.ToTable("NoLogEntity", "audittest");
        }
    }
}
