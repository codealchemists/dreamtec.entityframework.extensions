﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extensions.Connection
{
	public class ConnectionRetryExceptionEventArgs : EventArgs
	{
		public Exception Exception { get; set; }
        public TimeSpan Delay { get; set; }
        public int Retry { get; set; }

        public ConnectionRetryExceptionEventArgs(Exception exception, TimeSpan delay, int retry)
		{
            Exception = exception;
            Delay = delay;
            Retry = retry;
		}
	}
}
