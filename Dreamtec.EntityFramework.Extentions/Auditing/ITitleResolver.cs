﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
    /// <summary>
	/// Resolves a Title for an Entity
	/// </summary>
    public interface ITitleResolver
    {
        /// <summary>
		/// Resolves a Entity to a title
		/// </summary>
		/// <typeparam name="T">The <see cref="Type"/> of the Entity</typeparam>
		/// <param name="entity">The entity to resolve a title for</param>
		/// <returns>The title of the Entity</returns>
        string Resolve(IAuditableEntity entity);
    }
}
