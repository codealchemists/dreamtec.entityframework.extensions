using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Dreamtec.EntityFramework.Extensions.Auditing.Model.Mapping
{
	internal class JournalEntryRelatedEntityMap : EntityTypeConfiguration<JournalEntryRelatedEntity>
	{
		public JournalEntryRelatedEntityMap()
		{
			this.HasKey(t => new { t.JournalEntryId, t.EntityClassTypeId, t.EntityId } );

			this.Property(t => t.JournalEntryId)
				.IsRequired();

			this.Property(t => t.EntityClassTypeId)
				.IsRequired();

			this.Property(t => t.EntityId)
				.IsRequired();

			this.ToTable("JournalEntryRelatedEntity", "audit");

			this.HasRequired(t => t.EntityClassType)
				.WithMany()
				.HasForeignKey(t => t.EntityClassTypeId);

			this.HasRequired(t => t.JournalEntry)
				.WithMany(t => t.JournalEntryRelatedEntities)
				.HasForeignKey(t => t.JournalEntryId);
		}
	}
}
