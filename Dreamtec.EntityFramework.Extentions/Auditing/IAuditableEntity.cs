﻿using System;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
    /// <summary>
    /// An Entity that is auditable. 
    /// </summary>
    /// <remarks>
    /// An audit log will be written automatically for all these Entities that implements <see cref="IAuditableEntity"/> if their 
    /// <see cref="DbContext"/> inherits from <see cref="ChangeCaptureDbContext"/>.
    /// </remarks>
	public interface IAuditableEntity
	{
		Guid Id { get; set; }
	}
}

