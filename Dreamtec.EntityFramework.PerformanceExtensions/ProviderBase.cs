﻿using ResQ.Beryllium.ArgumentValidation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.PerformanceExtensions
{
	/// <summary>
	/// Class was copied and pasted from https://efbulkinsert.codeplex.com/
	/// </summary>
	public abstract class ProviderBase<TConnection, TTransaction> : IBulkInsertProvider
		where TConnection : IDbConnection
		where TTransaction : IDbTransaction
	{

		/// <summary>
		/// Current DbContext
		/// </summary>
		public DbContext Context
		{
			get;
		}

		public ProviderBase(DbContext context)
		{
			Validate.Argument(context, nameof(context))
				.IsNotNull();

			Context = context;
		}

		public BulkInsertOptions Options { get; set; }

		/// <summary>
		/// Connection string which current dbcontext is using
		/// </summary>
		protected virtual string ConnectionString
		{
			get
			{
				return (string)DbConnection.GetPrivateFieldValue("_connectionString");
			}
		}

		protected virtual IDbConnection DbConnection
		{
			get { return Context.Database.Connection; }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IDbConnection GetConnection()
		{
			return CreateConnection();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		protected abstract TConnection CreateConnection();

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entities"></param>
		/// <param name="transaction"></param>
		public void Run<T>(IEnumerable<T> entities, IDbTransaction transaction)
		{
			Validate.Argument(entities, nameof(entities))
				.IsNotNull()
				.AndArgument(transaction, nameof(transaction))
				.IsNotNull() ;

			Run(entities, (TTransaction)transaction);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entities"></param>
		public virtual void Run<T>(IEnumerable<T> entities)
		{

			Validate.Argument(entities, nameof(entities))
				.IsNotNull();

			using (var dbConnection = GetConnection())
			{
				dbConnection.Open();

				using (var transaction = dbConnection.BeginTransaction())
				{
					try
					{
						Run(entities, transaction);
						transaction.Commit();
					}
					catch (Exception)
					{
						if (transaction.Connection != null)
						{
							transaction.Rollback();
						}
						throw;
					}
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entities"></param>
		/// <param name="transaction"></param>
		public abstract void Run<T>(IEnumerable<T> entities, TTransaction transaction);
	}
}