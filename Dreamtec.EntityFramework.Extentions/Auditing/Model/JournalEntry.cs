using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System;


namespace Dreamtec.EntityFramework.Extensions.Auditing.Model
{
	public partial class JournalEntry
	{
        public virtual int Id { get; set; }
		public virtual DateTimeOffset DateApplied { get; set; }
		public virtual string UserComment { get; set; }
		public virtual string SystemComment { get; set; }
        public virtual Verbosity Verbosity { get; set; }
        public virtual Confidentiality Confidentiality { get; set; }
		public virtual string ActionOwningObjectName { get; set; }
		public virtual string ActionName { get; set; }

		public virtual Guid TransactionId { get; set; }
        public virtual Transaction Transaction { get; set; }

		public virtual ICollection<JournalEntryRelatedEntity> JournalEntryRelatedEntities { get; set; } = new HashSet<JournalEntryRelatedEntity>();
	}
}
