using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System;


namespace Dreamtec.EntityFramework.Extensions.Auditing.Model
{
	public class EntityClassType
	{
		public virtual int Id { get; set; }
		public virtual string FullTypeName { get; set; }
		public virtual bool IsStale { get; set; }
		public virtual bool IsDeleted { get; set; }
	}
}
