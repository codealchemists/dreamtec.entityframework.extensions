using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntityFramework.Metadata.Exceptions
{
	/// <summary>
	/// https://github.com/schneidenbach/EntityFramework.Metadata
	/// </summary>
	public class ParentNotMappedYetException : Exception
    {
    }
}
