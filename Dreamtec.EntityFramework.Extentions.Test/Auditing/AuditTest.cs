﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Dreamtec.EntityFramework.Extensions.Auditing;
using Dreamtec.EntityFramework.Extensions.Auditing.Model;
using Dreamtec.EntityFramework.Extentions.Test.Auditing.Model;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using Entity = Dreamtec.EntityFramework.Extentions.Test.Auditing.Model.Entity;
using System.Diagnostics;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing
{
    public class AuditTest
    {
        private static string AUDIT_TEST_DB_CONNECTION = "AuditTestDbContext";
        private static int THREADING_TEST_PARALLELISM = 999;

        private static readonly List<string> _iAuditableFieldsToExclude = typeof(IAuditableEntity).GetProperties().Select(p => p.Name).ToList();

        public class AuditContext : AuditTest
        {
            [Fact]
            public void ShouldNotAuditLog()
            {
                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION) { ShouldAuditLog = false } )
                {
                    var auditContextCapturer = new TestAuditContextCapturer(dbContext);

                    Guid loginId = Guid.NewGuid();

					auditContextCapturer.LoggedInId = loginId;
					auditContextCapturer.Audit(this);
					auditContextCapturer.LoggedInId = Guid.NewGuid();
					auditContextCapturer.Audit(this);

                    Guid vehicleId = Guid.NewGuid();

                    Vehicle vehicle = createVehicle(vehicleId);

                    Guid personId = Guid.NewGuid();

                    Person person = createPerson(personId);

                    vehicle.Person = person;

                    dbContext.Vehicles.Add(vehicle);

                    dbContext.SaveChanges();

                    using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                    {
                        Transaction transaction = auditLogDbContext.Transactions.Where(t => t.LoginId == loginId).FirstOrDefault();

                        transaction.Should().BeNull();
                    }
                }
            }

            [Fact]
            public void ShouldNotOverrideFirstAuditContext()
            {
                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    var auditContextCapturer = new TestAuditContextCapturer(dbContext);

                    Guid loginId = Guid.NewGuid();

					auditContextCapturer.LoggedInId = loginId;
					auditContextCapturer.Audit(this);
					auditContextCapturer.LoggedInId = Guid.NewGuid();
					auditContextCapturer.Audit(this);

                    Guid vehicleId = Guid.NewGuid();

                    Vehicle vehicle = createVehicle(vehicleId);

                    Guid personId = Guid.NewGuid();

                    Person person = createPerson(personId);

                    vehicle.Person = person;

                    dbContext.Vehicles.Add(vehicle);

                    dbContext.SaveChanges();

                    using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                    {
                        Transaction transaction = auditLogDbContext.Transactions.Where(t => t.LoginId == loginId).FirstOrDefault();

                        transaction.Should().NotBeNull();
                    }
                }
            }
        }

        public class Add : AuditTest
        {
            [Fact]
            public void ShouldBeThreadSafe()
            {
                Parallel.For(0, THREADING_TEST_PARALLELISM, index =>
                {
                    using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                    {
                        Guid loggedInId = Guid.NewGuid();

                        var auditContextCapturer = new TestAuditContextCapturer(dbContext);

						auditContextCapturer.LoggedInId = loggedInId;
						auditContextCapturer.Audit(this);

                        Person person = createPerson(Guid.NewGuid());

                        dbContext.People.Add(person);

                        dbContext.SaveChanges();

                        using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                        {
                            auditLogDbContext.Transactions.Where(t => t.LoginId == loggedInId).FirstOrDefault().Should().NotBeNull();
                        }
                    }
                });
;            }

            [Fact]
            public void ShouldRollbackEverythingWhenAuditingFails()
            {
                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    var auditContextCapturer = new TestAuditContextCapturer(dbContext);

					auditContextCapturer.LoggedInId = Guid.NewGuid();
					//We re on purpose giving a large value, so that we get the audit save to fail
					auditContextCapturer.Audit(this, "verylong,morethan250charslong.verylong,morethan250charslong.verylong,morethan250charslong.verylong,morethan250charslong.verylong,morethan250charslong.verylong,morethan250charslong.verylong,morethan250charslong.verylong,morethan250charslong.verylong,morethan250charslong.verylong,morethan250charslong.verylong,morethan250charslong.verylong,morethan250charslong.verylong,morethan250charslong.verylong,morethan250charslong.");

                    Guid vehicleId = Guid.NewGuid();

                    Vehicle vehicle = createVehicle(vehicleId);

                    Guid personId = Guid.NewGuid();

                    Person person = createPerson(personId);

                    vehicle.Person = person;

                    dbContext.Vehicles.Add(vehicle);

                    try
                    {
                        dbContext.SaveChanges();
                    }
                    catch (Exception)
                    {
                        /* 
                         * We expected the exception
                         * Now check that we don't have orphaned data
                         */
                        using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                        {
                            auditLogDbContext.Entities.Where(e => e.AuditableEntityId == vehicleId).FirstOrDefault().Should().BeNull();
                            auditLogDbContext.Entities.Where(e => e.AuditableEntityId == personId).FirstOrDefault().Should().BeNull();
                        }
                        dbContext.Vehicles.Where(v => v.Id == vehicleId).FirstOrDefault().Should().BeNull();
                        dbContext.People.Where(p => p.Id == personId).FirstOrDefault().Should().BeNull();
                    }
                }
            }

            [Fact]
            public void ShouldRollbackEverythingWhenEntityCaptureFails()
            {
                Guid vehicleId = Guid.NewGuid();

                Vehicle vehicle = createVehicle(vehicleId);
                vehicle.Color = "verylong,morethan100chars.verylong,morethan100chars.verylong,morethan100chars.verylong,morethan100chars.verylong,morethan100chars.verylong,morethan100chars.verylong,morethan100chars.verylong,morethan100chars.";

                Guid personId = Guid.NewGuid();

                Person person = createPerson(personId);

                vehicle.Person = person;

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.Vehicles.Add(vehicle);

                    try
                    {
                        dbContext.SaveChanges();
                    }
                    catch (Exception)
                    {
                        /* 
                         * We expected the exception
                         * Now check that we don't have orphaned data
                         */

                        using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                        {
                            auditLogDbContext.Entities.Where(e => e.AuditableEntityId == vehicleId).FirstOrDefault().Should().BeNull();
                            auditLogDbContext.Entities.Where(e => e.AuditableEntityId == personId).FirstOrDefault().Should().BeNull();
                        }
                        dbContext.Vehicles.Where(v => v.Id == vehicleId).FirstOrDefault().Should().BeNull();
                        dbContext.People.Where(p => p.Id == personId).FirstOrDefault().Should().BeNull();
                    }
                }
            }

            [Fact]
            public void ShouldReuseSameClassNameAcrossSchemasWithEF()
            {

                /*
                 * Create a Class of named "Entity", which will on purpose clash with Dreamtec.EntityFramework.Extensions.Auditing.Model.Entity.
                 * This new class is also Auditable, so it will generate an Audit log, in effect creating an [audit].Entity.
                 * When saving this class under [audittest].Entity, it should save (without getting confused) to [audit].Entity
                 */
                Entity testEntity = new Entity();

                Guid entityId = Guid.NewGuid();

                testEntity.Id = entityId;

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.Entities.Add(testEntity);

                    dbContext.SaveChanges();

                    dbContext.Entities.Where(e => e.Id == entityId).FirstOrDefault().Should().NotBeNull();
                    using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                    {
                        auditLogDbContext.Entities.Where(e => e.AuditableEntityId == entityId).FirstOrDefault().Should().NotBeNull();
                    }
                }
            }

            [Fact]
            public void ShouldNotSaveContextState()
            {
                Guid personId = Guid.NewGuid();
                Person person = createPerson(personId);
                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {

                    dbContext.People.Add(person);

                    dbContext.SaveChanges();

                    using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                    {
                        Transaction transaction = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == personId).Select(e => e.Transaction).FirstOrDefault();

                        transaction.Should().NotBeNull();
                        transaction.LoginId.HasValue.Should().BeFalse();
                        transaction.UserIpAddress.Should().BeNull();
                        transaction.TriggerActionOwningObjectName.Should().BeNull();
                        transaction.TriggerActionName.Should().BeNull();
                        transaction.EndpointName.Should().BeNull();
                    }
                }
            }

            [Fact]
            public void ShouldSaveContextState()
            {
                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    Guid loggedInId = Guid.NewGuid();

                    var auditContextCapturer = new TestAuditContextCapturer(dbContext);

					auditContextCapturer.LoggedInId = loggedInId;
					auditContextCapturer.Audit(this);

                    Person person = createPerson(Guid.NewGuid());

                    dbContext.People.Add(person);

                    dbContext.SaveChanges();

                    using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                    {
                        Transaction transaction = auditLogDbContext.Transactions.Where(t => t.LoginId == loggedInId).FirstOrDefault();

                        transaction.Should().NotBeNull();
                        transaction.TriggerActionOwningObjectName.Should().Be(this.GetType().Name);
                        transaction.TriggerActionName.Should().Be(MethodBase.GetCurrentMethod().Name);
                    }
                }
            }

			[Fact]
			public void ShouldSaveVersion()
			{
				string version = "1.0.0.0";

				using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION) { VersionResolver = new TestVersionResolver() { Version = version } })
				{
					Guid personId = Guid.NewGuid();

					Person person = createPerson(personId);

					dbContext.People.Add(person);

					dbContext.SaveChanges();

					using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
					{
						Extensions.Auditing.Model.Entity entity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == personId).FirstOrDefault();

						entity.Should().NotBeNull();
						entity.Version.Should().Be(version);
					}
				}
			}

			[Fact]
			public void ShouldSaveJournalEntriesWithoutAudit()
			{
				using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION) { EntityClassIdResolver = new TestEntityClassIdResolver(AUDIT_TEST_DB_CONNECTION) })
				{
					var auditContextCapturer = new TestAuditContextCapturer(dbContext);

					Guid personId = Guid.NewGuid();

					Person person = createPerson(personId);

					string userComment = "testtesttest";

					auditContextCapturer.Journal(person, Verbosity.Normal, Confidentiality.Confidential, userComment);

					dbContext.SaveChanges();

					using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
					{
						var journalEntry = auditLogDbContext.JournalEntries.Where(je => je.UserComment == userComment);
						journalEntry.Should().NotBeEmpty();
					}
				}
			}


			[Fact]
            public void ShouldSaveJournalEntries()
            {
                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION) { EntityClassIdResolver = new TestEntityClassIdResolver(AUDIT_TEST_DB_CONNECTION) })
                {
                    Guid loggedInId = Guid.NewGuid();

                    var auditContextCapturer = new TestAuditContextCapturer(dbContext);

					auditContextCapturer.LoggedInId = loggedInId;
					auditContextCapturer.Audit(this);

					string userComment1 = "something secret";
                    string systemComment1 = "something said";

                    string userComment2 = "something else secret";
                    string systemComment2 = "something else said";

					var entityTypes = typeof(Person).Assembly.GetTypes()
						.Where(o => o.IsPublic && !o.IsAbstract && !o.IsInterface && o.IsClass
						&& o.FullName.Contains("Model"));

					AddToKnownTypes(entityTypes.ToArray());

					Guid personId = Guid.NewGuid();
					Guid vehicleId = Guid.NewGuid();

					Person person = createPerson(personId);
					Vehicle vehicle = createVehicle(vehicleId);
					vehicle.Person = person;

					IJournalableEntity[] associatedTypes = { person };
					
					auditContextCapturer.Journal(person, Verbosity.Normal, Confidentiality.Confidential, userComment1, systemComment1)
                        .Journal(vehicle, Verbosity.Verbose, Confidentiality.Confidential, userComment2, systemComment2, associatedTypes);

                    dbContext.Vehicles.Add(vehicle);
					dbContext.Database.Log= e => Debug.WriteLine(e);
                    dbContext.SaveChanges();

					var methodName = MethodBase.GetCurrentMethod().Name;

					using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                    {
						Transaction transaction = auditLogDbContext.Transactions.Include(t=> t.JournalEntries.Select(je => je.JournalEntryRelatedEntities.Select(jer => jer.EntityClassType))).Where(t => t.LoginId == loggedInId).FirstOrDefault();

                        transaction.Should().NotBeNull();
						transaction.JournalEntries.Should().NotBeEmpty();

						transaction.JournalEntries.ToList().ForEach(j =>
                        {
							if (!(j.ActionOwningObjectName == typeof(Person).Name || j.ActionOwningObjectName == typeof(Vehicle).Name))
							{
								throw new ArgumentOutOfRangeException($"The ActionOwningObjectName should be either {typeof(Person).Name} or {typeof(Vehicle).Name}");
							}

							j.ActionName.Should().Be(methodName);

							if (j.Verbosity == Verbosity.Normal)
                            {
                                j.UserComment.Should().Be(userComment1);
                                j.SystemComment.Should().Be(systemComment1);
                            } else if (j.Verbosity == Verbosity.Verbose)
                            {
                                j.UserComment.Should().Be(userComment2);
                                j.SystemComment.Should().Be(systemComment2);
								j.JournalEntryRelatedEntities.Should().NotBeEmpty();

								j.JournalEntryRelatedEntities.ToList().ForEach(r =>
								{
									r.EntityId.Should().Should().NotBeNull();
									
									if (r.EntityId == personId)
									{
										r.EntityClassType.FullTypeName.Should().Be(typeof(Person).FullName);
									}

									if (r.EntityId == vehicleId)
									{
										r.EntityClassType.FullTypeName.Should().Be(typeof(Vehicle).FullName);
									}

								});
                            }
                        });
                    }
                }
            }

			[Fact]
			public void ShouldFireEvents()
			{
				using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION) { EntityClassIdResolver = new TestEntityClassIdResolver(AUDIT_TEST_DB_CONNECTION) })
				{
					dbContext.OnBeforeJournalPersist += (sender,e) => 
					{
						sender.Should().NotBeNull();
						e.Should().NotBeNull();
						e.JournalEntries.Should().NotBeNull();
						e.JournalEntries.Should().NotBeEmpty();
						e.Transaction.Should().NotBeNull();
					};

					dbContext.OnAfterJournalPersist += (sender, e) =>
					{
						sender.Should().NotBeNull();
						e.Should().NotBeNull();
						e.JournalEntries.Should().NotBeNull();
						e.JournalEntries.Should().NotBeEmpty();
						e.Transaction.Should().NotBeNull();
					};

					dbContext.OnBeforeAuditPersist += (sender, e) =>
					{
						sender.Should().NotBeNull();
						e.Should().NotBeNull();
						e.Transaction.Should().NotBeNull();
					};

					dbContext.OnAfterAuditPersist += (sender, e) =>
					{
						sender.Should().NotBeNull();
						e.Should().NotBeNull();
						e.Transaction.Should().NotBeNull();
					};

					Guid loggedInId = Guid.NewGuid();

					var auditContextCapturer = new TestAuditContextCapturer(dbContext);

					string userComment1 = "something secret";
					string systemComment1 = "something said";

					string userComment2 = "something else secret";
					string systemComment2 = "something else said";

					var entityTypes = typeof(Person).Assembly.GetTypes()
						.Where(o => o.IsPublic && !o.IsAbstract && !o.IsInterface && o.IsClass
						&& o.FullName.Contains("Model"));

					AddToKnownTypes(entityTypes.ToArray());

					Guid personId = Guid.NewGuid();
					Guid vehicleId = Guid.NewGuid();

					Person person = createPerson(personId);
					Vehicle vehicle = createVehicle(vehicleId);
					vehicle.Person = person;

					IJournalableEntity[] associatedTypes = { person };

					auditContextCapturer.LoggedInId = loggedInId;
					auditContextCapturer.Journal(person, Verbosity.Normal, Confidentiality.Confidential, userComment1, systemComment1)
						.Journal(vehicle, Verbosity.Verbose, Confidentiality.Confidential, userComment2, systemComment2, associatedTypes);

					dbContext.Vehicles.Add(vehicle);

					dbContext.SaveChanges();

					
				}
			}

			[Fact]
	        public void ShouldLogWithTitleResolver()
	        {
				Guid vehicleId = Guid.NewGuid();

				Vehicle vehicle = createVehicle(vehicleId);

				Guid personId = Guid.NewGuid();

				Person person = createPerson(personId);

				vehicle.Person = person;

				var title = "Test Title";

				using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION) { TitleResolver = new TestTitleResolver() {Title = title } })
				{
					dbContext.Vehicles.Add(vehicle);

					dbContext.SaveChanges();

					using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
					{
						Extensions.Auditing.Model.Entity personAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == personId).FirstOrDefault();

						personAuditEntity.Should().NotBeNull();
						personAuditEntity.EntityTitle.Should().Be(title);

						Extensions.Auditing.Model.Entity vehicleAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == vehicleId).FirstOrDefault();

						vehicleAuditEntity.Should().NotBeNull();
						vehicleAuditEntity.EntityTitle.Should().Be(title);
					}
				}
			}


	        [Fact]
            public void ShouldLogWithTypeResolver()
            {
                Guid vehicleId = Guid.NewGuid();

                Vehicle vehicle = createVehicle(vehicleId);

                Guid personId = Guid.NewGuid();

                Person person = createPerson(personId);

                vehicle.Person = person;

                var returnType = typeof(string);

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION) { TypeResolver = new TestTypeResolver(returnType) })
                {
                    dbContext.Vehicles.Add(vehicle);

                    dbContext.SaveChanges();

                    using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                    {
                        Extensions.Auditing.Model.Entity personAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == personId).FirstOrDefault();

                        personAuditEntity.Should().NotBeNull();
                        personAuditEntity.FullEntityTypeName.Should().Be(returnType.FullName);

                        Extensions.Auditing.Model.Entity vehicleAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == vehicleId).FirstOrDefault();

                        vehicleAuditEntity.Should().NotBeNull();
                        vehicleAuditEntity.FullEntityTypeName.Should().Be(returnType.FullName);
                    }
                }
            }

            [Fact]
            public void ShouldNotLogIAuditableWithExclude()
            {
                NoLogExcludedEntity nogLogEntity = new NoLogExcludedEntity();

                Guid noLogEntityId = Guid.NewGuid();

                nogLogEntity.Id = noLogEntityId;

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.NoLogExcludedEntities.Add(nogLogEntity);

                    dbContext.SaveChanges();

                    dbContext.NoLogEntities.Where(n => n.Id == noLogEntityId).Should().NotBeNull();

                    using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                    {
                        auditLogDbContext.Entities.Where(e => e.AuditableEntityId == noLogEntityId).FirstOrDefault().Should().BeNull();
                        auditLogDbContext.Entities.Where(e => e.FullEntityTypeName == typeof(NoLogEntity).FullName).FirstOrDefault().Should().BeNull();
                    }
                }
            }

            [Fact]
            public void ShouldNotLogNonIAuditable()
            {
                NoLogEntity nogLogEntity = new NoLogEntity();

                Guid noLogEntityId = Guid.NewGuid();

                nogLogEntity.Id = noLogEntityId;

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.NoLogEntities.Add(nogLogEntity);

                    dbContext.SaveChanges();

                    dbContext.NoLogEntities.Where(n => n.Id == noLogEntityId).Should().NotBeNull();

                    using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                    {
                        auditLogDbContext.Entities.Where(e => e.AuditableEntityId == noLogEntityId).FirstOrDefault().Should().BeNull();
                        auditLogDbContext.Entities.Where(e => e.FullEntityTypeName == typeof(NoLogEntity).FullName).FirstOrDefault().Should().BeNull();
                    }
                }
            }

            [Fact]
            public void ShouldSupportUnicode()
            {
                //Japenese chars
                string username = GenerateUnicodeString(50, 0x4e00, 0x4f80);

                Guid vehicleId = Guid.NewGuid();

                Vehicle vehicle = createVehicle(vehicleId);

                Guid personId = Guid.NewGuid();

                Person person = createPerson(personId);
                person.Name = username;

                vehicle.Person = person;

                Guid loggedInId = Guid.NewGuid();

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    var auditContextCapturer = new TestAuditContextCapturer(dbContext);

					auditContextCapturer.LoggedInId = loggedInId;
					auditContextCapturer.Audit(this);
                    auditContextCapturer.SetUsername(username);

                    dbContext.Vehicles.Add(vehicle);

                    dbContext.SaveChanges();

                    dbContext.People.Where(e => e.Id == personId).FirstOrDefault().Name.Should().Be(username);
                }

                using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    Transaction transaction = auditLogDbContext.Transactions.Where(t => t.LoginId == loggedInId).FirstOrDefault();

                    transaction.Should().NotBeNull();
                    transaction.UserName.Should().Be(username);

                    Extensions.Auditing.Model.Entity personAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == personId).FirstOrDefault();

                    personAuditEntity.Should().NotBeNull();
                    personAuditEntity.EntityProperties.Should().NotBeEmpty();
                    personAuditEntity.EntityProperties.ToList().ForEach(p =>
                    {
                        p.ModifiedValue.Should().NotBeNull();

                        switch (p.PropertyName)
                        {
                            case "Name":
                                p.ModifiedValue.Should().Be(username);
                                break;
                            case "Surname":
                            case "Age":
                                //not testing them now, but they are expected properties
                                break;
                            default:
                                throw new ArgumentOutOfRangeException("Unknown property [" + p.PropertyName + "] for entity");
                        }
                    });
                }
            }

            [Fact]
            public void ShouldNotLogExcludedProperties()
            {
                Person person1 = new Person();

                Guid personId = Guid.NewGuid();

                person1.Id = personId;
                string name = "speedster";
                person1.Name = name;
                string surname = "lovesbmw";
                person1.Surname = surname;
                person1.BirthdayDate = DateTime.Now;

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.People.Add(person1);

                    dbContext.SaveChanges();
                }

                using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    Extensions.Auditing.Model.Entity personAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == personId).FirstOrDefault();

                    personAuditEntity.Should().NotBeNull();
                    personAuditEntity.EntityProperties.Should().NotBeEmpty();
                    /*
                     * Get all properties that are excluded, they should not be contained.
                     * Note this won't work if some of the properties are Null, Null properties aren't written to the audit log
                    */
                    personAuditEntity.EntityProperties.Select(p => p.PropertyName).Should()
                        .NotContain(
                            typeof(Vehicle).GetProperties()
                            .Where(p => p.GetCustomAttributes(typeof(ExcludeFromAuditingAttribute), false).Any())
                            .Select(p => p.Name)
                        );
                }
            }

            [Fact]
            public void ShouldNotLogNonMappedProperties()
            {
                Person person1 = new Person();

                Guid personId = Guid.NewGuid();

                person1.Id = personId;
                string name = "speedster";
                person1.Name = name;
                string surname = "lovesbmw";
                person1.Surname = surname;
                person1.BirthdayDate = DateTime.Now;
                person1.SomeRuntimeVar = "testtest";

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.People.Add(person1);

                    dbContext.SaveChanges();
                }

                using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    Extensions.Auditing.Model.Entity personAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == personId).FirstOrDefault();

                    personAuditEntity.Should().NotBeNull();
                    personAuditEntity.EntityProperties.Should().NotBeEmpty();
                    /*
                     * Get all properties that are excluded, they should not be contained.
                     * Note this won't work if some of the properties are Null, Null properties aren't written to the audit log
                    */
                    personAuditEntity.EntityProperties.Select(p => p.PropertyName).Should().NotContain(new string[] { "SomeRuntimeVar" });
                }
            }

            [Fact]
            public void ShouldLogAdditions()
            {
                Guid vehicleId = Guid.NewGuid();

                Vehicle vehicle = createVehicle(vehicleId);

                Guid personId = Guid.NewGuid();

                Person person = createPerson(personId);

                vehicle.Person = person;

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.Vehicles.Add(vehicle);

                    dbContext.SaveChanges();
                }

                using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    Extensions.Auditing.Model.Entity vehicleAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == vehicleId).FirstOrDefault();
                    Extensions.Auditing.Model.Entity personAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == personId).FirstOrDefault();

                    vehicleAuditEntity.Should().NotBeNull();
                    vehicleAuditEntity.FullEntityTypeName.Should().Be(typeof(Vehicle).FullName);
                    vehicleAuditEntity.DbOperation.Should().Be("I");
                    vehicleAuditEntity.EntityProperties.Should().NotBeEmpty();
                    vehicleAuditEntity.EntityProperties.ToList().ForEach(p =>
                    {
                        p.DataType.Should().NotBeNullOrEmpty();
                        p.PropertyName.Should().NotBeNullOrEmpty();
                    });

                    personAuditEntity.Should().NotBeNull();
                    personAuditEntity.FullEntityTypeName.Should().Be(typeof(Person).FullName);
                    personAuditEntity.DbOperation.Should().Be("I");
                    personAuditEntity.EntityProperties.Should().NotBeEmpty();
                    personAuditEntity.EntityProperties.ToList().ForEach(p =>
                    {
                        p.DataType.Should().NotBeNullOrEmpty();
                        p.PropertyName.Should().NotBeNullOrEmpty();
                    });
                }
            }

            [Fact]
            public void ShouldCreateEntityProperties()
            {
                Vehicle vehicle = new Vehicle();

                Guid vehicleId = Guid.NewGuid();

                vehicle.Id = vehicleId;
                string black = "black";
                vehicle.Color = black;
                string make = "bmw";
                vehicle.Make = make;
                string vehicleName = "m3";
                vehicle.Name = vehicleName;
                byte wheels = 4;
                vehicle.Wheels = wheels;
                vehicle.Price = 300000;

                Person person1 = new Person();

                Guid personId = Guid.NewGuid();

                person1.Id = personId;
                string name = "speedster";
                person1.Name = name;
                string surname = "lovesbmw";
                person1.Surname = surname;
                person1.BirthdayDate = DateTime.Now;

                vehicle.Person = person1;

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.Vehicles.Add(vehicle);

                    dbContext.SaveChanges();
                }

                using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    Extensions.Auditing.Model.Entity vehicleAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == vehicleId).FirstOrDefault();
                    Extensions.Auditing.Model.Entity personAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == personId).FirstOrDefault();

                    vehicleAuditEntity.Should().NotBeNull();
                    vehicleAuditEntity.EntityProperties.Should().NotBeEmpty();
                    /*
                     * Get all properties that are not excluded, that are value types and isn't a property on IAuditableEntity (e.g. Id)
                     * Note this won't work if some of the properties are Null, Null properties aren't written to the audit log
                    */
                    vehicleAuditEntity.EntityProperties.Select(p => p.PropertyName).Should()
                        .Contain(
                            typeof(Vehicle).GetProperties()
                            .Where(p => !p.GetCustomAttributes(typeof(ExcludeFromAuditingAttribute), false).Any()
                                && p.PropertyType.IsValueType
                                && !_iAuditableFieldsToExclude.Contains(p.Name))
                            .Select(p => p.Name)
                        );

                    personAuditEntity.Should().NotBeNull();
                    personAuditEntity.EntityProperties.Should().NotBeEmpty();
                    /*
                      * Get all properties that are not excluded, that are value types and isn't a property on IAuditableEntity (e.g. Id)
                      * Note this won't work if some of the properties are Null, Null properties aren't written to the audit log
                     */
                    personAuditEntity.EntityProperties.Select(p => p.PropertyName).Should()
                        .Contain(
                            typeof(Person).GetProperties()
                            .Where(p => !p.GetCustomAttributes(typeof(ExcludeFromAuditingAttribute), false).Any()
                                && p.PropertyType.IsValueType
                                && !_iAuditableFieldsToExclude.Contains(p.Name))
                            .Select(p => p.Name)
                        );
                }
            }

            [Fact]
            public void ShouldCreateEntityPropertiesWithCorrectModifiedValues()
            {
                Vehicle vehicle = new Vehicle();

                Guid vehicleId = Guid.NewGuid();

                vehicle.Id = vehicleId;
                string black = "black";
                vehicle.Color = black;
                string make = "bmw";
                vehicle.Make = make;
                string vehicleName = "m3";
                vehicle.Name = vehicleName;
                byte wheels = 4;
                vehicle.Wheels = wheels;

                Person person1 = new Person();

                Guid personId = Guid.NewGuid();

                person1.Id = personId;
                string name = "speedster";
                person1.Name = name;
                string surname = "lovesbmw";
                person1.Surname = surname;

                vehicle.Person = person1;

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.Vehicles.Add(vehicle);

                    dbContext.SaveChanges();
                }

                using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    Extensions.Auditing.Model.Entity vehicleAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == vehicleId).FirstOrDefault();
                    Extensions.Auditing.Model.Entity personAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == personId).FirstOrDefault();

                    vehicleAuditEntity.Should().NotBeNull();
                    vehicleAuditEntity.EntityProperties.Should().NotBeEmpty();
                    vehicleAuditEntity.EntityProperties.ToList().ForEach(p =>
                    {
                        p.ModifiedValue.Should().NotBeNull();

                        switch (p.PropertyName)
                        {
                            case "Color":
                                p.ModifiedValue.Should().Be(black);
                                break;
                            case "Make":
                                p.ModifiedValue.Should().Be(make);
                                break;
                            case "Name":
                                p.ModifiedValue.Should().Be(vehicleName);
                                break;
                            case "Wheels":
                                p.ModifiedValue.Should().Be(wheels.ToString());
                                break;
                            case "ManufacturedDate":
                            case "Produced":
                            case "Price":
                            case "PersonId":
                            //not testing them now, but they are expected properties
                            break;
                            default:
                                throw new ArgumentOutOfRangeException("Unknown property [" + p.PropertyName + "] for entity");
                        }
                    });

                    personAuditEntity.Should().NotBeNull();
                    personAuditEntity.EntityProperties.Should().NotBeEmpty();
                    personAuditEntity.EntityProperties.ToList().ForEach(p =>
                    {
                        p.ModifiedValue.Should().NotBeNull();

                        switch (p.PropertyName)
                        {
                            case "Surname":
                                p.ModifiedValue.Should().Be(surname);
                                break;
                            case "Name":
                                p.ModifiedValue.Should().Be(name);
                                break;
                            case "Age":
                            //not testing them now, but they are expected properties
                            break;
                            default:
                                throw new ArgumentOutOfRangeException("Unknown property [" + p.PropertyName + "] for entity");
                        }
                    });
                }
            }

            [Fact]
            public void ShouldSupportDatatypes()
            {
                Vehicle vehicle = new Vehicle();

                Guid vehicleId = Guid.NewGuid();

                vehicle.Id = vehicleId;
                string black = "longertext,longertext";
                vehicle.Color = black;
                string make = "longertext,longertext,longertextlongertext";
                vehicle.Make = make;
                string vehicleName = "longertext,longertext,longertext";
                vehicle.Name = vehicleName;
                byte wheels = byte.MaxValue;
                vehicle.Wheels = wheels;
                int price = int.MaxValue;
                vehicle.Price = price;
                long produced = long.MaxValue;
                vehicle.Produced = produced;
                DateTimeOffset manufacturedDate = DateTime.Now;
                vehicle.ManufacturedDate = manufacturedDate;

                Person person1 = new Person();

                Guid personId = Guid.NewGuid();

                person1.Id = personId;
                string name = "longertext,longertext,longertextlonger";
                person1.Name = name;
                string surname = "longertext";
                person1.Surname = surname;

                vehicle.Person = person1;

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.Vehicles.Add(vehicle);

                    dbContext.SaveChanges();
                }

                using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    Extensions.Auditing.Model.Entity vehicleAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == vehicleId).FirstOrDefault();

                    vehicleAuditEntity.Should().NotBeNull();
                    vehicleAuditEntity.EntityProperties.Should().NotBeEmpty();
                    vehicleAuditEntity.EntityProperties.ToList().ForEach(p =>
                    {
                        p.ModifiedValue.Should().NotBeNull();

                        switch (p.PropertyName)
                        {
                            case "Color":
                                p.ModifiedValue.Should().Be(black);
                                break;
                            case "Make":
                                p.ModifiedValue.Should().Be(make);
                                break;
                            case "Name":
                                p.ModifiedValue.Should().Be(vehicleName);
                                break;
                            case "Wheels":
                                p.ModifiedValue.Should().Be(wheels.ToString());
                                break;
                            case "ManufacturedDate":
                                p.ModifiedValue.Should().Be(manufacturedDate.ToString());
                                break;
                            case "Produced":
                                p.ModifiedValue.Should().Be(produced.ToString());
                                break;
                            case "Price":
                                p.ModifiedValue.Should().Be(price.ToString());
                                break;
                            case "PersonId":
                            //not testing them now, but they are expected properties
                            break;
                            default:
                                throw new ArgumentOutOfRangeException("Unknown property [" + p.PropertyName + "] for entity");
                        }
                    });
                }
            }

            [Fact]
            public void ShouldHaveNullBeforeValue()
            {
                Guid vehicleId = Guid.NewGuid();

                Vehicle vehicle = createVehicle(vehicleId);

                Guid personId = Guid.NewGuid();

                Person person = createPerson(personId);

                vehicle.Person = person;

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.Vehicles.Add(vehicle);

                    dbContext.SaveChanges();
                }

                using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    Extensions.Auditing.Model.Entity vehicleAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == vehicleId).FirstOrDefault();
                    Extensions.Auditing.Model.Entity personAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == personId).FirstOrDefault();

                    vehicleAuditEntity.Should().NotBeNull();
                    vehicleAuditEntity.EntityProperties.Should().NotBeEmpty();
                    vehicleAuditEntity.EntityProperties.ToList().ForEach(p => p.OriginalValue.Should().BeNull());
                }
            }
        }
        
        public class Update : AuditTest
        {
            [Fact]
            public void ShouldHaveBeforeAndAfterValues()
            {
                Guid vehicleId = Guid.NewGuid();

                Vehicle vehicle = createVehicle(vehicleId);

                Guid personId = Guid.NewGuid();

                Person person = createPerson(personId);

                vehicle.Person = person;

                //Modify existing entity and save again
                String originalName = vehicle.Name;
                String newName = "SomethingElse";

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.Vehicles.Add(vehicle);

                    dbContext.SaveChanges();
                   
                    vehicle.Name = newName;

                    dbContext.SaveChanges();
                }

                using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    //Get the UPDATE, because the INSERT happened against the same vehicleId
                    Extensions.Auditing.Model.Entity vehicleAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == vehicleId && e.DbOperation == "U").FirstOrDefault();
                    vehicleAuditEntity.Should().NotBeNull();
                    vehicleAuditEntity.EntityProperties.Should().NotBeEmpty();

                    vehicleAuditEntity.EntityProperties.ToList().ForEach(p =>
                    {
                        p.OriginalValue.Should().NotBeNull();
                        p.ModifiedValue.Should().NotBeNull();

                        switch (p.PropertyName)
                        {
                            case "Name":
                                p.OriginalValue.Should().Be(originalName);
                                p.ModifiedValue.Should().Be(newName);
                                break;
                            default:
                                break;
                        }
                    });
                }
            }

            [Fact]
            public void ShouldHaveNullAfterValue()
            {
                Guid vehicleId = Guid.NewGuid();

                Vehicle vehicle = createVehicle(vehicleId);
                vehicle.Price = int.MaxValue;

                Guid personId = Guid.NewGuid();

                Person person = createPerson(personId);

                vehicle.Person = person;

                //Modify existing entity and save again
                int? originalPrice = vehicle.Price;

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.Vehicles.Add(vehicle);

                    dbContext.SaveChanges();
                   
                    vehicle.Price = null;

                    dbContext.SaveChanges();
                }

                using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    //Get the UPDATE, because the INSERT happened against the same vehicleId
                    Extensions.Auditing.Model.Entity vehicleAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == vehicleId && e.DbOperation == "U").FirstOrDefault();
                    vehicleAuditEntity.Should().NotBeNull();
                    vehicleAuditEntity.EntityProperties.Should().NotBeEmpty();

                    vehicleAuditEntity.EntityProperties.ToList().ForEach(p =>
                    {
                        p.OriginalValue.Should().NotBeNull();
                        p.ModifiedValue.Should().BeNull();

                        switch (p.PropertyName)
                        {
                            case "Price":
                                p.OriginalValue.Should().Be(originalPrice.Value.ToString());
                                break;
                            default:
                                break;
                        }
                    });
                }
            }
        }

        [TestClass]
        public class Delete : AuditTest
        {
            [Fact]
            public void ShouldLogDeletions()
            {
                Guid vehicleId = Guid.NewGuid();

                Vehicle vehicle = createVehicle(vehicleId);
                vehicle.Price = int.MaxValue;

                Guid personId = Guid.NewGuid();

                Person person = createPerson(personId);

                vehicle.Person = person;

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.Vehicles.Add(vehicle);

                    dbContext.SaveChanges();

                    dbContext.Vehicles.Remove(vehicle);

                    dbContext.SaveChanges();
                }

                using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    //Get the DELETE, because the INSERT happened against the same vehicleId
                    Extensions.Auditing.Model.Entity vehicleAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == vehicleId && e.DbOperation == "D").FirstOrDefault();
                    vehicleAuditEntity.Should().NotBeNull();
                    vehicleAuditEntity.EntityProperties.Should().NotBeEmpty();
                }
            }

            [Fact]
            public void ShouldHaveNullAfterValue()
            {
                Guid vehicleId = Guid.NewGuid();

                Vehicle vehicle = createVehicle(vehicleId);
                vehicle.Price = int.MaxValue;

                Guid personId = Guid.NewGuid();

                Person person = createPerson(personId);

                vehicle.Person = person;

                using (var dbContext = new AuditTestDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    dbContext.Vehicles.Add(vehicle);

                    dbContext.SaveChanges();

                    dbContext.Vehicles.Remove(vehicle);

                    dbContext.SaveChanges();
                }

                using (var auditLogDbContext = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
                {
                    //Get the DELETE, because the INSERT happened against the same vehicleId
                    Extensions.Auditing.Model.Entity vehicleAuditEntity = auditLogDbContext.Entities.Where(e => e.AuditableEntityId == vehicleId && e.DbOperation == "D").FirstOrDefault();
                    vehicleAuditEntity.Should().NotBeNull();
                    vehicleAuditEntity.EntityProperties.Should().NotBeEmpty();

                    vehicleAuditEntity.EntityProperties.ToList().ForEach(p =>
                    {
                        p.OriginalValue.Should().NotBeNull();
                        p.ModifiedValue.Should().BeNull();
                    });
                }
            }
        }

        private Vehicle createVehicle(Guid id)
        {
            Vehicle vehicle = new Vehicle();
            
            vehicle.Id = id;
            vehicle.Color = GenerateString(50);
            vehicle.Make = GenerateString(50);
            vehicle.Name = GenerateString(50);
            vehicle.Wheels = 4;

            return vehicle;
        }

        private Person createPerson(Guid id)
        {
            Person person = new Person();

            person.Id = id;
            person.Name = GenerateString(50);
            person.Surname = GenerateString(50);

            return person;
        }

		private void AddToKnownTypes(params Type[] types)
		{
			IDictionary<string, Type> typeAndFullNames = types.ToDictionary(t => t.FullName, x => x);

			IList<Type> toBeInserted = types.ToList();

			IList<String> staleEntities = new List<string>();

			using (var context = new AuditLogDbContext(AUDIT_TEST_DB_CONNECTION))
			{
				//Find stale Types
				context.EntityClassTypes.ToList().ForEach(e =>
				{
					if (!typeAndFullNames.Keys.Contains(e.FullTypeName))
					{
						e.IsStale = true;
						staleEntities.Add(e.FullTypeName);
					}
					else
					{
						toBeInserted.Remove(typeAndFullNames[e.FullTypeName]);
					}
				});

				foreach (var type in toBeInserted)
				{
					string associatedEntityFullName = type.FullName;

					EntityClassType entityClassType = new EntityClassType { FullTypeName = associatedEntityFullName };
					context.EntityClassTypes.Add(entityClassType);
				}

				context.SaveChanges();
			}

			staleEntities.Should().BeEmpty("you need to update or fix the Types in [dbo].[EntityClassType] when you renamed or delete an Entity");
		}

		private const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        private Random random = new Random();

        private string GenerateString(int length)
        {
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private string GenerateUnicodeString(int length, int minCharCode, int maxCharCode)
        {
            var builder = new StringBuilder(length);
            for (var i = 0; i < length; i++)
            {
                builder.Append((char)random.Next(minCharCode, maxCharCode));
            }
            return builder.ToString();
        }
    }
}
