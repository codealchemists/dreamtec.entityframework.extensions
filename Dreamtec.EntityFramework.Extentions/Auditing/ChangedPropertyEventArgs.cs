﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
	public class EntitiesChangedEventArgs : EventArgs
	{
		public IEnumerable<ChangedEntity> ChangedEntities { get; }
		internal EntitiesChangedEventArgs(IEnumerable<ChangedEntity> changedEntities)
		{
			ChangedEntities = changedEntities;
		}
	}

	public class ChangedProperty : EventArgs
	{
		public string DataType { get; }
		public string ModifiedValue { get; }
		public string OriginalValue { get; }
		public string PropertyName { get; }
		
		internal ChangedProperty(string propertyName, string dataType, string originalValue, string modifiedValue)
		{
		
			PropertyName = propertyName;
			DataType = dataType;
			OriginalValue = originalValue;
			ModifiedValue = modifiedValue;

		}
	}

	public class ChangedEntity : EventArgs
	{

		public Guid AuditableEntityId { get; }
		public string DbOperation { get; }
		public string FullEntityTypeName { get; }
		public string EntityVersion { get; }
		public string EntityTitle { get; }
		public DbEntityEntry<IAuditableEntity> Entity { get; }
		public IEnumerable<ChangedProperty> ChangedProperties { get; }
		internal ChangedEntity(Guid auditableEntityId, string fullEntityTypeName, string entityVersion, string entityTitle, DbEntityEntry<IAuditableEntity> entity,string dbOperation, IEnumerable<ChangedProperty> changedProperties)
		{
			AuditableEntityId = auditableEntityId;
			FullEntityTypeName = fullEntityTypeName;
			DbOperation = dbOperation;
			EntityTitle = entityTitle;
			EntityVersion = entityVersion;
			Entity = entity;
			ChangedProperties = changedProperties;
		}
	}

}
