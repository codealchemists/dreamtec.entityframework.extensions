﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
    /// <summary>
    /// Resolves a <see cref="Type"/> to another <see cref="Type"/> during auditing.
    /// </summary>
    /// <remarks>
    /// You would want to resolve to a different <see cref="Type"/> when your Entities aren't returned as their original <see cref="Type"/>s. e.g. a Proxied Entity.
    /// </remarks>
    public interface ITypeResolver
    {
        /// <summary>
        /// Resolves a Entity <see cref="Type"/> to its actual <see cref="Type"/>
        /// </summary>
        /// <param name="type">The <see cref="Type"/> to resolve</param>
        /// <returns>The resolved actual <see cref="Type"/></returns>
        Type Resolve(Type type);
    }
}
