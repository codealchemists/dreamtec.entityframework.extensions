﻿using System;
using System.Data.Entity;
using System.Transactions;


namespace Dreamtec.EntityFramework.Extensions.Auditing
{




	public abstract partial class ChangeCaptureDbContext : DbContext
	{
		public class TransactionFailedArgs : EventArgs
		{
			public DbContext DbContext { get; }

			public TransactionEventArgs TransactionEventArgs { get; }

			internal TransactionFailedArgs(DbContext dbContext, TransactionEventArgs transactionEventArgs)
			{
				this.DbContext = dbContext;
				this.TransactionEventArgs = transactionEventArgs;
			}
		}

	}
}