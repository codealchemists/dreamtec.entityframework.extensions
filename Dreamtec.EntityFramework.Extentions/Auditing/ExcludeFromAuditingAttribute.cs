﻿using System;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
    /// <summary>
    /// Exclude a property on an Entity from auditing
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]
	public class ExcludeFromAuditingAttribute : Attribute
	{
	}
}
