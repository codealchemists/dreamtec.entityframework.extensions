using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System;


namespace Dreamtec.EntityFramework.Extensions.Auditing.Model
{
	public partial class Entity
	{
		private ICollection<EntityProperty> _entityProperties = new HashSet<EntityProperty>();

		public virtual Guid Id { get; set; }
		public virtual Guid AuditableEntityId { get; set; }
		public virtual string DbOperation { get; set; }
		public virtual string EntityTitle { get; set; }
		public virtual string FullEntityTypeName { get; set; }
		public virtual string Version { get; set; }

		public virtual Guid TransactionId { get; set; }
        public virtual Transaction Transaction { get; set; }

        public virtual ICollection<EntityProperty> EntityProperties { get { return _entityProperties; } set { _entityProperties = value; } }

		// Properties not mapped.
		public IAuditableEntity AuditableEntity { get; set; }
		public DbEntityEntry<IAuditableEntity> Entry { get; set; }
	}
}
