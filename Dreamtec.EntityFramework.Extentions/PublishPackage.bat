@echo off
NuGet.exe pack Dreamtec.EntityFramework.Extentions.csproj -IncludeReferencedProjects -Properties OutDir="bin\release"

nuget push *.nupkg -Source "c:\nugets"
del *.nupkg

pause