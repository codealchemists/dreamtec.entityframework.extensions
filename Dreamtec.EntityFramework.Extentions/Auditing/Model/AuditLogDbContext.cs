using Dreamtec.EntityFramework.Extensions.Auditing.Model.Mapping;
using System.Data.Entity;

namespace Dreamtec.EntityFramework.Extensions.Auditing.Model
{
	public partial class AuditLogDbContext : DbContext
	{
		static AuditLogDbContext()
		{
			Database.SetInitializer<AuditLogDbContext>(null);
		}

        public AuditLogDbContext(string nameOrConnectionString)
			: base(nameOrConnectionString)
		{
        }

        public DbSet<Transaction> Transactions { get; set; }
		public DbSet<Entity> Entities { get; set; }
		public DbSet<EntityProperty> EntityProperties { get; set; }
        public DbSet<JournalEntry> JournalEntries { get; set; }
		public DbSet<EntityClassType> EntityClassTypes { get; set; }
		public DbSet<JournalEntryRelatedEntity> JournalEntryRelatedEntities { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new TransactionMap());
			modelBuilder.Configurations.Add(new EntityMap());
			modelBuilder.Configurations.Add(new EntityPropertyMap());
            modelBuilder.Configurations.Add(new JournalEntryMap());
			modelBuilder.Configurations.Add(new EntityClassTypeMap());
			modelBuilder.Configurations.Add(new JournalEntryRelatedEntityMap());
		}
	}
}
