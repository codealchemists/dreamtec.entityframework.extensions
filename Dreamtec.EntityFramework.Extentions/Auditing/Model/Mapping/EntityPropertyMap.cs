using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Dreamtec.EntityFramework.Extensions.Auditing.Model.Mapping
{
	internal class EntityPropertyMap : EntityTypeConfiguration<EntityProperty>
	{
		public EntityPropertyMap()
		{
            this.HasKey(t => t.Id);

            this.Property(t => t.Id)
                .IsRequired();

            this.Property(t => t.EntityId)
                .IsRequired();

            this.Property(t => t.DataType)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.PropertyName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.OriginalValue)
                .HasMaxLength(4000);

            this.Property(t => t.ModifiedValue)
                .HasMaxLength(4000);

            this.ToTable("EntityProperty", "audit");

            this.HasRequired(t => t.Entity)
                .WithMany(t => t.EntityProperties)
                .HasForeignKey(d => d.EntityId);
		}
	}
}
