﻿using Dreamtec.EntityFramework.Extensions.Auditing.Model;
using Dreamtec.EntityFramework.PerformanceExtensions;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;


namespace Dreamtec.EntityFramework.Extensions.Auditing
{
	// Credit goes to the following sources of inspiration.
	// http://stackoverflow.com/questions/6156818/entity-framework-4-1-dbcontext-override-savechanges-to-audit-property-change
	// https://jmdority.wordpress.com/2011/07/20/using-entity-framework-4-1-dbcontext-change-tracking-for-audit-logging/

	/// <summary>
	/// A <see cref="DbContext"/> that will audit all changes for you.
	/// </summary>
	/// <remarks>
	/// Your Entities must impement the <see cref="IAuditableEntity"/> interface in order for them to be audited.
	/// </remarks>
	public abstract partial class ChangeCaptureDbContext : DbContext
	{
		private static readonly byte MAX_ENDPOINTNAME_LENGTH = 50;

		//cache for retrieved properties
		private static ConcurrentDictionary<Type, IEnumerable<string>> _propertiesToAuditPerTypeOfEntity = new ConcurrentDictionary<Type, IEnumerable<string>>();

		//properties to skip from IAuditableEntity
		private static List<string> _iAuditableFieldsToExclude = typeof(IAuditableEntity).GetProperties().Select(p => p.Name).ToList();

		private string _nameOrConnectionString;

		/// <summary>
		/// The <see cref="AuditContextState"/> to be used for auditing, when saving changes
		/// <seealso cref="ChangeCaptureDbContext.NewTransaction"/>
		/// </summary>
		public AuditContextState AuditContextState { get; set; }

		/// <summary>
		/// The <see cref="ITypeResolver"/> to resolve an Entity's <see cref="Type"/> with. If null will use the Entity's <see cref="Type"/> as is.
		/// <seealso cref="ITypeResolver"/>
		/// </summary>
		public ITypeResolver TypeResolver { get; set; }

		/// <summary>
		/// The <see cref="ITitleResolver"/> to resolve an Entity's title with. If null, title will be left empty
		/// </summary>
		public ITitleResolver TitleResolver { get; set; }

		/// <summary>
		/// The <see cref="IVersionResolver"/> to resolve and Entity's version with. if null, the version will be left empty
		/// </summary>
		public IVersionResolver VersionResolver { get; set; }

		/// <summary>
		/// The <see cref="IEntityClassIdResolver"/> to resolve and Entity's <see cref="EntityClassType"/> Id with. May not be null, will throw an exception
		/// </summary>
		public IEntityClassIdResolver EntityClassIdResolver { get; set; }

		public bool ShouldAuditLog { get; set; } = true;

		public IDictionary<string, object> Metadata { get; set; }

		#region Events

		public event EventHandler<AuditEventArgs> OnBeforeAuditPersist;
		public event EventHandler<AuditEventArgs> OnAfterAuditPersist;

		public event EventHandler<JournalEventArgs> OnBeforeJournalPersist;
		public event EventHandler<JournalEventArgs> OnAfterJournalPersist;

		public event EventHandler<BeforeSaveChangesEventArgs> OnBeforeSaveChanges = delegate { };
		public event EventHandler<AfterSaveChangesCommittedEventArgs> OnAfterSaveChangesCommitted = delegate { };

		public event EventHandler<EntitiesChangedEventArgs> OnChangeDetected = delegate { };
		public event EventHandler<TransactionFailedArgs> OnTransactionFailed;
		#endregion

		public ChangeCaptureDbContext(string nameOrConnectionString)
			: base(nameOrConnectionString)
		{
			_nameOrConnectionString = nameOrConnectionString;

			Metadata = new ConcurrentDictionary<string, object>();
		}

		#region Save

		public override int SaveChanges()
		{
			int resultCount;
			OnBeforeSaveChanges(this, new BeforeSaveChangesEventArgs(this));

			if (ShouldAuditLog)
			{
				Model.Transaction transaction = NewTransaction();

				/* The Transaction will lock, so do the majority of work beforehand
              * Then only save within a Transaction - All or nothing
              * We need to keep this locking time to a minimum
              */
				Changes changes = CaptureEntityChanges();

				Trace.WriteLine($"IncludedInAuditing:{changes.IncludedInAuditing.Count() },ExcludedFromAuditing,{changes.ExcludedFromAuditing.Count()},ChangeTrackingOnly:{changes.ChangeTrackingOnly.Count()}");
				AddToAudit(transaction, changes.IncludedInAuditing);

				//Capture all journal entries
				IList<JournalEntry> capturedJournalEntries = new List<JournalEntry>();
				if (AuditContextState != null)
				{
					capturedJournalEntries = CaptureJournalEntries(transaction, AuditContextState.JournalEntries);
					OnBeforeJournalPersist?.Invoke(this, new JournalEventArgs(transaction, capturedJournalEntries));

				}

				//Do not invoke Before event within the transaction - It could be costly to do other's work in tran
				OnBeforeAuditPersist?.Invoke(this, new AuditEventArgs(transaction));

				//Locking code
				using (var tranScope = JoinOrCreateTransactionScope(IsolationLevel.ReadCommitted))
				{
					//Save the Entities from the inherited context
					resultCount = base.SaveChanges();

					//Save the Audit Log
					SaveAuditableChanges(transaction);

					FlushJournals(AuditContextState);

					System.Transactions.Transaction.Current.TransactionCompleted += (sender, e) =>
					{
						//Only fire these event if we have a commited transaction
						if (e.Transaction.TransactionInformation.Status == TransactionStatus.Committed)
						{
							OnAfterSaveChangesCommitted(this, new AfterSaveChangesCommittedEventArgs(this));
							//After save fire the after persist event
							if (AuditContextState != null)
							{
								OnAfterJournalPersist?.Invoke(this, new JournalEventArgs(transaction, capturedJournalEntries));
							}

							OnAfterAuditPersist?.Invoke(this, new AuditEventArgs(transaction));
							OnChangeDetected(this, new EntitiesChangedEventArgs(changes.ChangeTrackingOnly));

						}
					};

					tranScope.Complete();
				}
			}
			else
			{
				//Locking code
				using (var tranScope = JoinOrCreateTransactionScope(IsolationLevel.ReadCommitted))
				{
					//Save the Entities from the inherited context
					resultCount = base.SaveChanges();


					System.Transactions.Transaction.Current.TransactionCompleted += (sender, e) =>
					{
						//Only fire these event if we have a commited transaction
						if (e.Transaction.TransactionInformation.Status == TransactionStatus.Committed)
						{
							OnAfterSaveChangesCommitted(this, new AfterSaveChangesCommittedEventArgs(this));

						}
					};

					tranScope.Complete();
				}

			}

			return resultCount;
		}

		public override async Task<int> SaveChangesAsync()
		{
			int resultCount;
			OnBeforeSaveChanges(this, new BeforeSaveChangesEventArgs(this));

			if (ShouldAuditLog)
			{
				Model.Transaction transaction = NewTransaction();

				/* The Transaction will lock, so do the majority of work beforehand
             * Then only save within a Transaction - All or nothing
             * We need to keep this locking time to a minimum
             */
				Changes changes = CaptureEntityChanges();

				Trace.WriteLine($"IncludedInAuditing:{changes.IncludedInAuditing.Count() },ExcludedFromAuditing,{changes.ExcludedFromAuditing.Count()},ChangeTrackingOnly:{changes.ChangeTrackingOnly.Count()}");
				AddToAudit(transaction, changes.IncludedInAuditing);

				// Capture all journal entries
				IList<JournalEntry> capturedJournalEntries = new List<JournalEntry>();
				if (AuditContextState != null)
				{
					capturedJournalEntries = CaptureJournalEntries(transaction, AuditContextState.JournalEntries);
					OnBeforeJournalPersist?.Invoke(this, new JournalEventArgs(transaction, capturedJournalEntries));
				}

				// Do not invoke "Before" event within the transaction; It could be costly to do other's work in tran
				OnBeforeAuditPersist?.Invoke(this, new AuditEventArgs(transaction));

				// Locking code
				using (var tranScope = JoinOrCreateTransactionScopeWithAsyncFlow(IsolationLevel.ReadCommitted))
				{
					// Save the Entities from the inherited context
					resultCount = await base.SaveChangesAsync().ConfigureAwait(false);

					//Save the Audit Log
					await SaveAuditableChangesAsync(transaction).ConfigureAwait(false);

					FlushJournals(AuditContextState);

					System.Transactions.Transaction.Current.TransactionCompleted += (sender, e) =>
					{
						// Only fire these events if we have a committed transaction
						if (e.Transaction.TransactionInformation.Status == TransactionStatus.Committed)
						{
							OnAfterSaveChangesCommitted(this, new AfterSaveChangesCommittedEventArgs(this));
							// After save, fire the after persist event
							if (AuditContextState != null)
							{
								OnAfterJournalPersist?.Invoke(this, new JournalEventArgs(transaction, capturedJournalEntries));
							}

							OnAfterAuditPersist?.Invoke(this, new AuditEventArgs(transaction));
							OnChangeDetected(this, new EntitiesChangedEventArgs(changes.ChangeTrackingOnly));

						}
						else
						{
							this.OnTransactionFailed((object)this, new TransactionFailedArgs((DbContext)this, e));
						}
					};

					tranScope.Complete();
				}
			}
			else
			{
				//Locking code
				using (var tranScope = JoinOrCreateTransactionScopeWithAsyncFlow(IsolationLevel.ReadCommitted))
				{
					//Save the Entities from the inherited context
					resultCount = await base.SaveChangesAsync().ConfigureAwait(false);


					System.Transactions.Transaction.Current.TransactionCompleted += (sender, e) =>
					{
						//Only fire these event if we have a commited transaction
						if (e.Transaction.TransactionInformation.Status == TransactionStatus.Committed)
						{
							OnAfterSaveChangesCommitted(this, new AfterSaveChangesCommittedEventArgs(this));

						}
					};

					tranScope.Complete();
				}

			}

			return resultCount;
		}

		private void FlushJournals(AuditContextState auditContextState)
		{
			if (auditContextState == null)
				return;
			foreach (JournalEntry journalEntry in auditContextState.JournalEntries)
			{
				if (journalEntry != null)
					journalEntry.Flushed = true;
			}
		}

		private static TransactionScope JoinOrCreateTransactionScope(IsolationLevel isolationLevel)
		{
			var transactionOptions = new TransactionOptions();
			transactionOptions.IsolationLevel = isolationLevel;
			transactionOptions.Timeout = TransactionManager.MaximumTimeout;
			return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
		}

		private static TransactionScope JoinOrCreateTransactionScopeWithAsyncFlow(IsolationLevel isolationLevel)
		{
			var transactionOptions = new TransactionOptions();
			transactionOptions.IsolationLevel = isolationLevel;
			transactionOptions.Timeout = TransactionManager.MaximumTimeout;
			return new TransactionScope(TransactionScopeOption.Required, transactionOptions, TransactionScopeAsyncFlowOption.Enabled);
		}

		private void SaveAuditableChanges(Model.Transaction transaction)
		{

			if (transaction == null || (!transaction.Entities.Any() && !transaction.JournalEntries.Any()))
				return;

			using (var auditLogModelContext = new AuditLogDbContext(_nameOrConnectionString))
			{

				foreach (Entity entity in transaction.Entities.ToList())
				{
					entity.TransactionId = transaction.Id;
					entity.AuditableEntityId = entity.AuditableEntity.Id;

					foreach (EntityProperty property in entity.EntityProperties.ToList())
					{
						property.EntityId = entity.Id;
					}
				}

				foreach (Model.JournalEntry entry in transaction.JournalEntries.ToList())
				{
					entry.TransactionId = transaction.Id;
					entry.Transaction = null;
				}

				System.Diagnostics.Debug.WriteLine(auditLogModelContext.Database.Connection.GetType().FullName);
				auditLogModelContext.BulkInsert(new[] { transaction });
				auditLogModelContext.BulkInsert(transaction.Entities);
				auditLogModelContext.BulkInsert(transaction.Entities.SelectMany(o => o.EntityProperties));

				//Cant bulk it since there is a identity index on it
				foreach (Model.JournalEntry entry in transaction.JournalEntries)
					auditLogModelContext.JournalEntries.Add(entry);


				auditLogModelContext.SaveChanges();
			}
		}

		private async Task SaveAuditableChangesAsync(Model.Transaction transaction)
		{

			if (transaction == null || (!transaction.Entities.Any() && !transaction.JournalEntries.Any()))
				return;

			using (var auditLogModelContext = new AuditLogDbContext(_nameOrConnectionString))
			{

				foreach (Entity entity in transaction.Entities.ToList())
				{
					entity.TransactionId = transaction.Id;
					entity.AuditableEntityId = entity.AuditableEntity.Id;

					foreach (EntityProperty property in entity.EntityProperties.ToList())
					{
						property.EntityId = entity.Id;
					}
				}

				foreach (Model.JournalEntry entry in transaction.JournalEntries.ToList())
				{
					entry.TransactionId = transaction.Id;
					entry.Transaction = null;
				}

				System.Diagnostics.Debug.WriteLine(auditLogModelContext.Database.Connection.GetType().FullName);
				auditLogModelContext.BulkInsert(new[] { transaction });
				auditLogModelContext.BulkInsert(transaction.Entities);
				auditLogModelContext.BulkInsert(transaction.Entities.SelectMany(o => o.EntityProperties));

				//Cant bulk it since there is a identity index on it
				foreach (Model.JournalEntry entry in transaction.JournalEntries)
					auditLogModelContext.JournalEntries.Add(entry);


				await auditLogModelContext.SaveChangesAsync().ConfigureAwait(false);
			}
		}

		#endregion Save

		#region Get Changes

		private void AddToAudit(Model.Transaction transaction, IEnumerable<ChangedEntity> changedEntities)
		{
			foreach (var entity in changedEntities)
			{
				Entity auditedEntity = new Entity();
				auditedEntity.Id = Guid.NewGuid();
				auditedEntity.Entry = entity.Entity;
				auditedEntity.AuditableEntity = entity.Entity.Entity;
				auditedEntity.Version = entity.EntityVersion;
				auditedEntity.FullEntityTypeName = entity.FullEntityTypeName;
				auditedEntity.EntityTitle = entity.EntityTitle;
				auditedEntity.DbOperation = entity.DbOperation;

				foreach (ChangedProperty changedProperty in entity.ChangedProperties)
				{
					EntityProperty entityProperty = new EntityProperty();
					entityProperty.Id = Guid.NewGuid();
					entityProperty.DataType = changedProperty.DataType;
					entityProperty.PropertyName = changedProperty.PropertyName;
					entityProperty.OriginalValue = changedProperty.OriginalValue;
					entityProperty.ModifiedValue = changedProperty.ModifiedValue;

					auditedEntity.EntityProperties.Add(entityProperty);
				}

				transaction.Entities.Add(auditedEntity);
			}
		}

		private Changes CaptureEntityChanges()
		{
			ChangeTracker.DetectChanges();

			//Get changed Entity && Entities that is IAuditableEntity (except if they are marked ExcludeFromAuditing)
			var entries = ChangeTracker.Entries<IAuditableEntity>().Where(
				e => e.State != EntityState.Unchanged
			);

			List<ChangedEntity> changeTrackingOnly = new List<ChangedEntity>();
			List<ChangedEntity> excludedFromAuditing = new List<ChangedEntity>();
			List<ChangedEntity> includedInAuditing = new List<ChangedEntity>();

			foreach (var entry in entries)
			{
				var entityType = entry.Entity.GetType();

				var propertiesWithValues = GetProperties(entry.State == EntityState.Deleted ? entry.OriginalValues : entry.CurrentValues, entityType);

				if (entry.State == EntityState.Modified)
				{
					propertiesWithValues = propertiesWithValues.Where(propertyName => entry.Property(propertyName).IsModified);
				}

				List<ChangedProperty> changedProperties = new List<ChangedProperty>();
				foreach (var propertyName in propertiesWithValues)
				{
					object modifiedValue = null;

					if (entry.State == EntityState.Added || entry.State == EntityState.Modified)
					{
						modifiedValue = entry.CurrentValues[propertyName];
					}

					object originalValue = null;

					if (entry.State == EntityState.Modified || entry.State == EntityState.Deleted)
					{
						originalValue = entry.OriginalValues[propertyName];
					}

					string modifiedValueAsString = (modifiedValue == null) ? null : modifiedValue.ToString();
					string originalValueAsString = (originalValue == null) ? null : originalValue.ToString();

					if (originalValueAsString == modifiedValueAsString)
						continue;

					var typeDiscoveryValue = modifiedValue ?? originalValue;

					changedProperties.Add(new ChangedProperty(propertyName, typeDiscoveryValue.GetType().Name, originalValueAsString, modifiedValueAsString));
				}

				entityType = GetFullEntityTypeName(entry);

				if (!entry.Entity.GetType().GetCustomAttributes(typeof(ExcludeFromAuditingAttribute), true).Any() && !entry.Entity.GetType().GetCustomAttributes(typeof(ChangeTrackingOnlyAttribute), true).Any())
					includedInAuditing.Add(new ChangedEntity(entry.Entity.Id, entityType.FullName, VersionResolver?.ResolveVersion(entityType), GetTitle(entry.Entity), entry, EntityStateToString(entry.State), changedProperties));
				if (!entry.Entity.GetType().GetCustomAttributes(typeof(ExcludeFromAuditingAttribute), true).Any())
					changeTrackingOnly.Add(new ChangedEntity(entry.Entity.Id, entityType.FullName, VersionResolver?.ResolveVersion(entityType), GetTitle(entry.Entity), entry, EntityStateToString(entry.State), changedProperties));
				else
					excludedFromAuditing.Add(new ChangedEntity(entry.Entity.Id, entityType.FullName, VersionResolver?.ResolveVersion(entityType), GetTitle(entry.Entity), entry, EntityStateToString(entry.State), changedProperties));

			}

			return new Changes(changeTrackingOnly, excludedFromAuditing, includedInAuditing);
		}

		private string EntityStateToString(EntityState state)
		{
			switch (state)
			{
				case EntityState.Added:
					return "I";
				case EntityState.Deleted:
					return "D";
				case EntityState.Modified:
					return "U";
				default:
					throw new ArgumentException("EntityState [" + state + "] not supported");
			}
		}

		private IEnumerable<string> GetProperties(DbPropertyValues entryValues, Type entityType)
		{
			//check if the cache has the value first
			if (_propertiesToAuditPerTypeOfEntity.ContainsKey(entityType))
			{
				return _propertiesToAuditPerTypeOfEntity[entityType];
			}
			else
			{
				IEnumerable<string> properties = entityType.GetProperties()
				.Where(
					//Don't add excluded properties
					p => !p.GetCustomAttributes(typeof(ExcludeFromAuditingAttribute), true).Any()
					//only add value types (string isn't one, but should be treated as one)
					&& (p.PropertyType.IsValueType || p.PropertyType == typeof(string))
					//skip any properties from the IAuditableEntity
					&& !_iAuditableFieldsToExclude.Contains(p.Name)
					//Double check if the value is mapped by EF, if not remove it
					//e.g. a runtime var will not be in here, so don't audit it
					&& entryValues.PropertyNames.Contains(p.Name))
				.Select(p => p.Name)
				//Force execution to now - we don't want the above running everytime we loop over the collection
				//ToList() will in effect take the results and snapshot them to memory
				.ToList();

				_propertiesToAuditPerTypeOfEntity.TryAdd(entityType, properties);

				return properties;
			}
		}

		private IList<JournalEntry> CaptureJournalEntries(Model.Transaction transaction, IList<JournalEntry> journalEntries)
		{
			var entries = new List<JournalEntry>();
			foreach (var journalEntry in journalEntries.Where(o => !o.Flushed))
			{
				entries.Add(journalEntry);
				Model.JournalEntry modelJournalEntry = new Model.JournalEntry()
				{
					DateApplied = DateTimeOffset.Now,
					Verbosity = journalEntry.Verbosity,
					Confidentiality = journalEntry.Confidentiality,
					UserComment = journalEntry.UserComment,
					SystemComment = journalEntry.SystemComment,
					ActionName = journalEntry.ActionName,
					ActionOwningObjectName = journalEntry.ActionOwningObjectName,
					Transaction = transaction
				};

				if (journalEntry.AssociatedEntities != null)
				{
					using (var context = new AuditLogDbContext(_nameOrConnectionString))
					{
						foreach (var associatedEntity in journalEntry.AssociatedEntities)
						{
							string associatedEntityFullName;

							if (TypeResolver != null)
							{
								associatedEntityFullName = TypeResolver.Resolve(associatedEntity.GetType()).FullName;
							}
							else
							{
								associatedEntityFullName = associatedEntity.GetType().FullName;
							}

							if (EntityClassIdResolver == null)
							{
								throw new NullReferenceException("The EntityClassIdResolver may not be null");
							}

							int id = EntityClassIdResolver.ResolveId(associatedEntityFullName);

							modelJournalEntry.JournalEntryRelatedEntities.Add(new JournalEntryRelatedEntity()
							{
								EntityClassTypeId = id,
								EntityId = associatedEntity.Id
							});
						}
					}
				}

				transaction.JournalEntries.Add(modelJournalEntry);
			}

			return journalEntries;
		}

		#endregion Get Changes

		#region Create Entities

		private Model.Transaction NewTransaction()
		{
			Model.Transaction transaction = new Model.Transaction { Id = Guid.NewGuid(), DateApplied = DateTimeOffset.Now };

			if (AuditContextState != null)
			{
				if (AuditContextState.TriggerActionName != null)
					transaction.TriggerActionName = AuditContextState.TriggerActionName;
				if (AuditContextState.TriggerActionOwningObjectName != null)
					transaction.TriggerActionOwningObjectName = AuditContextState.TriggerActionOwningObjectName;
				if (AuditContextState.UserIpAddress != null)
					transaction.UserIpAddress = AuditContextState.UserIpAddress;
				if (AuditContextState.Username != null)
					transaction.UserName = AuditContextState.Username;
				if (AuditContextState.LoginId.HasValue)
					transaction.LoginId = AuditContextState.LoginId.Value;
				if (AuditContextState.EndpointName != null)
				{
					transaction.EndpointName = new string(AuditContextState.EndpointName.Take(MAX_ENDPOINTNAME_LENGTH).ToArray());
				}
			}
			transaction.ExecutingMachineName = Environment.MachineName;

			return transaction;
		}

		private Entity NewTransientEntity(
			DbEntityEntry<IAuditableEntity> entry,
			Type typeOfEntity,
			string DbOperation)
		{
			var auditedEntity = new Entity();
			auditedEntity.Id = Guid.NewGuid();
			auditedEntity.Entry = entry;
			auditedEntity.AuditableEntity = entry.Entity;

			Type entityType = GetFullEntityTypeName(entry);
			auditedEntity.Version = VersionResolver?.ResolveVersion(entityType);
			auditedEntity.FullEntityTypeName = entityType.FullName;
			auditedEntity.EntityTitle = GetTitle(entry.Entity);
			auditedEntity.DbOperation = DbOperation;

			return auditedEntity;
		}

		private Type GetFullEntityTypeName(DbEntityEntry<IAuditableEntity> entry)
		{
			Type type = entry.Entity.GetType();

			if (TypeResolver != null)
			{
				type = TypeResolver.Resolve(type);

				return type;
			}
			else
			{
				return type;
			}

		}

		private string GetTitle(IAuditableEntity auditableEntity)
		{
			return TitleResolver != null ? TitleResolver.Resolve(auditableEntity) : null;
		}

		#endregion Create Entities

		private sealed class Changes
		{
			internal IEnumerable<ChangedEntity> ChangeTrackingOnly { get; }
			internal IEnumerable<ChangedEntity> ExcludedFromAuditing { get; }
			internal IEnumerable<ChangedEntity> IncludedInAuditing { get; }
			internal Changes(IEnumerable<ChangedEntity> changeTrackingOnly, IEnumerable<ChangedEntity> excludedFromAuditing, IEnumerable<ChangedEntity> includedInAuditing)
			{
				ChangeTrackingOnly = changeTrackingOnly;
				ExcludedFromAuditing = excludedFromAuditing;
				IncludedInAuditing = includedInAuditing;
			}
		}

	}
}