﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
    /// <summary>
    /// The <see cref="IAuditContextCapturer"/> has the responsibility of gathering audit related information.
    /// As a part of capturing the information the <see cref="IAuditContextCapturer"/> must 
    /// also save it to the <see cref="ChangeCaptureDbContext"/>'s AuditContextState property.
    /// </summary>
    /// <remarks>
    /// During the <see cref="ChangeCaptureDbContext.SaveChanges"/> of the <see cref="ChangeCaptureDbContext"/>, 
    /// the <see cref="AuditContextState"/> will be used to enrich the audit log.
    /// </remarks>
    public interface IAuditContextCapturer
    {
        ChangeCaptureDbContext DbContext { get; set; }

        /// <summary>
        /// Captures the audit information, for saving to the <see cref="ChangeCaptureDbContext"/>
        /// </summary>
        /// <param name="triggerActionOwningObject">The object from where the audit originated</param>
        /// <param name="triggerActionName">The method that caused the audit to start</param>
        /// <returns>The same <see cref="IAuditContextCapturer"/> for a fluent api</returns>
        IAuditContextCapturer Audit(object triggerActionOwningObject, [CallerMemberName] string triggerActionName = null);

		/// <summary>
		/// Captures the journal information, for saving to the <see cref="ChangeCaptureDbContext"/>
		/// </summary>
		/// <param name="actionOwningObject">The object from where the journal originated</param>
		/// <param name="verbosity">The <see cref="Verbosity"/> of the journal entry</param>
		/// <param name="confidentiality">The <see cref="Confidentiality"/> of the journal entry</param>
		/// <param name="userComment">The user comment of the journal entry</param>
		/// <param name="systemComment">The system comment of the journal entry</param>
		/// <param name="actionName">The method that caused the journal entry</param>
		/// <param name="additionalAssociatedEntities">Entities additionally associated (actionOwningObject is auto-associated) to the Journal</param>
		/// <remarks>
		/// On Audit we only capture the entry point with the actionOwningObject and actionName (thus trigger prepended) once.
		/// With Journal we capture the entry point with the actionOwningObject and actionName every time.
		/// </remarks>
		/// <returns>The same <see cref="IAuditContextCapturer"/> for a fluent api</returns>
		IAuditContextCapturer Journal(IJournalableEntity actionOwningObject, Verbosity verbosity, Confidentiality confidentiality, string userComment = null, string systemComment = null, IJournalableEntity[] additionalAssociatedEntities = null, [CallerMemberName] string actionName = null);
	}
}
