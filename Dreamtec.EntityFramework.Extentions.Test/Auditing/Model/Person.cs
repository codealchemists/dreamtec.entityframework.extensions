using System;
using System.Collections.Generic;
using Dreamtec.EntityFramework.Extensions.Auditing;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing.Model
{
    public class Person : IAuditableEntity, IJournalableEntity
	{
        private ICollection<Vehicle> _vehicles = new HashSet<Vehicle>();

        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }
        public virtual byte Age { get; set; }
        [ExcludeFromAuditing]
        public virtual DateTime? BirthdayDate { get; set; }
        public virtual ICollection<Vehicle> Vehicles { get { return _vehicles; } set { _vehicles = value; } }

        //ignore this property
        public string SomeRuntimeVar { get; set; }
    }
}
