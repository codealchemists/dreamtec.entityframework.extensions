using System;
using Dreamtec.EntityFramework.Extensions.Auditing;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing.Model
{
	public partial class Vehicle : IAuditableEntity, IJournalableEntity
	{
        public virtual Guid Id { get; set; }
        public virtual Guid PersonId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Make { get; set; }
        public virtual string Color { get; set; }
        public virtual byte Wheels { get; set; }
        public virtual DateTimeOffset ManufacturedDate { get; set; }
        public virtual long Produced { get; set; }
        public virtual int? Price { get; set; }

        // Properties not mapped.
        public Person Person { get; set; }
    }
}
