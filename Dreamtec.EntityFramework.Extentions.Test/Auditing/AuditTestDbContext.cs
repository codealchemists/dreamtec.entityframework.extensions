﻿using System.Data.Entity;
using Dreamtec.EntityFramework.Extensions.Auditing;
using Dreamtec.EntityFramework.Extentions.Test.Auditing.Model;
using Dreamtec.EntityFramework.Extentions.Test.Auditing.Model.Mapping;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing
{
    public class AuditTestDbContext : ChangeCaptureDbContext
    {
		static AuditTestDbContext()
		{
			Database.SetInitializer<AuditTestDbContext>(null);
		}

		public AuditTestDbContext(string nameOrConnectionString)
			: base(nameOrConnectionString)
		{
			Database.SetInitializer<AuditTestDbContext>(null);
		}

		public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<Entity> Entities { get; set; }
        public DbSet<NoLogEntity> NoLogEntities { get; set; }
        public DbSet<NoLogExcludedEntity> NoLogExcludedEntities { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new VehicleMap());
            modelBuilder.Configurations.Add(new PersonMap());
            modelBuilder.Configurations.Add(new EntityMap());
            modelBuilder.Configurations.Add(new NoLogEntityMap());
            modelBuilder.Configurations.Add(new NoLogExcludedEntityMap());
        }
    }
}
