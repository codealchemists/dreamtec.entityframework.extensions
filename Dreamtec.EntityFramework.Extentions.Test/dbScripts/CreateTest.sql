CREATE SCHEMA [audittest]
GO

CREATE TABLE [audittest].[NoLogExcludedEntity](
	[Id] [uniqueidentifier] NULL
) ON [PRIMARY]
GO

CREATE TABLE [audittest].[NoLogEntity](
	[Id] [uniqueidentifier] NULL
) ON [PRIMARY]
GO

CREATE TABLE [audittest].[Entity](
	[Id] [uniqueidentifier] NULL
) ON [PRIMARY]
GO

CREATE TABLE [audittest].[Person](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[Age] [tinyint] NOT NULL,
	[BirthdayDate] [datetime2](4) NULL,
 CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [audittest].[Vehicle](
	[Id] [uniqueidentifier] NOT NULL,
	[PersonId] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Make] [nvarchar](50) NOT NULL,
	[Color] [nvarchar](50) NOT NULL,
	[Wheels] [tinyint] NOT NULL,
	[ManufacturedDate] [datetimeoffset](4) NOT NULL,
	[Produced] [bigint] NOT NULL,
	[Price] [int] NULL,
 CONSTRAINT [PK_Vehicle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [audittest].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Vehicle_Person] FOREIGN KEY([PersonId])
REFERENCES [audittest].[Person] ([Id])
GO

ALTER TABLE [audittest].[Vehicle] CHECK CONSTRAINT [FK_Vehicle_Person]
GO

