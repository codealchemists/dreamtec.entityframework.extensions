using System;
using Dreamtec.EntityFramework.Extensions.Auditing;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing.Model
{
    public class Entity : IAuditableEntity
    {
        public virtual Guid Id { get; set; }
    }
}
