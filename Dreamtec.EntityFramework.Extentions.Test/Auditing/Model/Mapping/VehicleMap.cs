using System.Data.Entity.ModelConfiguration;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing.Model.Mapping
{
	internal class VehicleMap : EntityTypeConfiguration<Vehicle>
    {
        public VehicleMap()
        {
            this.HasKey(t => t.Id);

            this.Property(t => t.Id)
                .IsRequired();

            this.Property(t => t.PersonId)
                .IsRequired();

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Make)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Color)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Wheels)
                .IsRequired();

            this.Property(t => t.ManufacturedDate)
                .IsRequired();

            this.Property(t => t.Produced)
                .IsRequired();

            this.Property(t => t.Price);

            this.ToTable("Vehicle", "audittest");
        }
    }
}
