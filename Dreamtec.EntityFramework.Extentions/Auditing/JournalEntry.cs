using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System;


namespace Dreamtec.EntityFramework.Extensions.Auditing
{
	public partial class JournalEntry
	{
        public string UserComment { get; set; }
		public string SystemComment { get; set; }
        public Verbosity Verbosity { get; set; }
        public Confidentiality Confidentiality { get; set; }
		public string ActionName { get; set; }
		public string ActionOwningObjectName { get; set; }
		public IList<IJournalableEntity> AssociatedEntities { get; set; }
		public bool Flushed { get; set; }
	}
}
