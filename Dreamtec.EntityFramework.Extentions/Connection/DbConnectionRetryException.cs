﻿using System;

namespace Dreamtec.EntityFramework.Extensions.Connection
{
    [Serializable]
    public class DbConnectionRetryException : EntityFrameworkExtensionException
    {
        public int Retry { get; set; }

        public DbConnectionRetryException(int retry)
        {
            Retry = retry;
        }

        public DbConnectionRetryException(int retry,
            string message)
            : base(message)
        {
            Retry = retry;
        }

        public DbConnectionRetryException(int retry,
            string message,
            Exception innerException)
            : base(message, innerException)
        {
            Retry = retry;
        }

        protected DbConnectionRetryException(int retry,
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
            Retry = retry;
        }
    }
}
