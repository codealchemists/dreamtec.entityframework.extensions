﻿using Dreamtec.EntityFramework.Extensions.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing
{
	public class TestTitleResolver : ITitleResolver
	{
		public string Title { get; set; }

		public string Resolve(IAuditableEntity entity)
		{
			return Title;
		}
	}
}
