﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.PerformanceExtensions
{

	[Serializable]
	public sealed class PreformanceExtensionsException : Exception
	{
		public PreformanceExtensionsException() { }
		public PreformanceExtensionsException(string message) : base(message) { }
		public PreformanceExtensionsException(string message, Exception inner) : base(message, inner) { }
		
	}
}
