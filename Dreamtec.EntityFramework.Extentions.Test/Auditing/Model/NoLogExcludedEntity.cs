using System;
using Dreamtec.EntityFramework.Extensions.Auditing;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing.Model
{
    [ExcludeFromAuditing]
    public class NoLogExcludedEntity : IAuditableEntity
    {
        public virtual Guid Id { get; set; }
    }
}
