﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
	public class AuditEventArgs : EventArgs
	{
		public Model.Transaction Transaction { get; set; }

		public AuditEventArgs(Model.Transaction transaction)
		{
			this.Transaction = transaction;
		}
	}
}
