﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
	/// <summary>
	/// Resolves a <see cref="Type"/> to its version
	/// </summary>
	public interface IVersionResolver
	{
		/// <summary>
		/// Resolves a Entity <see cref="Type"/> to its version
		/// </summary>
		/// <param name="type">The <see cref="Type"/> to resolve</param>
		/// <returns>The resolved assembly version</returns>
		string ResolveVersion(Type type);
	}
}
