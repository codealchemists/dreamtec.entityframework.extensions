﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.PerformanceExtensions
{
	/// <summary>
	/// Class was copied and pasted from https://efbulkinsert.codeplex.com/
	/// </summary>
	public sealed class BulkInsertOptions
	{
		public int BatchSize { get; set; } = 50;
		public SqlBulkCopyOptions SqlBulkCopyOptions { get; set; } = SqlBulkCopyOptions.Default;
		public int TimeOutSeconds { get;  set; } = 30;
	}
}
