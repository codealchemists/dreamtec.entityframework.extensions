using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Dreamtec.EntityFramework.Extensions.Auditing.Model.Mapping
{
	internal class TransactionMap : EntityTypeConfiguration<Transaction>
	{
		public TransactionMap()
		{
            this.HasKey(t => t.Id);

            this.Property(t => t.Id)
                .IsRequired();

            this.Property(t => t.DateApplied)
                .IsRequired();

            this.Property(t => t.LoginId);

            this.Property(t => t.UserName)
                .HasMaxLength(250);

            this.Property(t => t.UserIpAddress)
                .HasMaxLength(45);

            this.Property(t => t.TriggerActionOwningObjectName)
                .HasMaxLength(250);

            this.Property(t => t.TriggerActionName)
                .HasMaxLength(250);

            this.Property(t => t.ExecutingMachineName)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.EndpointName)
                .HasMaxLength(250);

            this.ToTable("Transaction", "audit");
        }
	}
}
