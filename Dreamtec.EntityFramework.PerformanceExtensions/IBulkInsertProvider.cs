﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.PerformanceExtensions
{
	/// <summary>
	/// Class was copied and pasted from https://efbulkinsert.codeplex.com/
	/// </summary>
	public interface IBulkInsertProvider
	{
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		IDbConnection GetConnection();

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entities"></param>
		void Run<T>(IEnumerable<T> entities);

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entities"></param>
		/// <param name="transaction"></param>
		void Run<T>(IEnumerable<T> entities, IDbTransaction transaction);

		/// <summary>
		/// Current DbContext
		/// </summary>
		DbContext Context { get; }

		/// <summary>
		/// 
		/// </summary>
		BulkInsertOptions Options { get; set; }
	}
}