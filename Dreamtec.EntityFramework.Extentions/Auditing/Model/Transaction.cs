using System;
using System.Collections.Generic;

namespace Dreamtec.EntityFramework.Extensions.Auditing.Model
{
	public partial class Transaction
	{
        public virtual Guid Id { get; set; }
		public virtual DateTimeOffset DateApplied { get; set; }
		public virtual Guid? LoginId { get; set; }
		public virtual string UserName { get; set; }
		public virtual string UserIpAddress { get; set; }
		public virtual string TriggerActionOwningObjectName { get; set; }
		public virtual string TriggerActionName { get; set; }
		public virtual string ExecutingMachineName { get; set; }
		public virtual string EndpointName { get; set; }
		public virtual ICollection<Entity> Entities { get; set; } = new HashSet<Entity>();
		public virtual ICollection<JournalEntry> JournalEntries { get; set; } = new HashSet<JournalEntry>();
	}
}
