﻿using System;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;
using System.Transactions;
using Polly;

namespace Dreamtec.EntityFramework.Extensions.Connection
{
	/// <summary>
	/// A strategy that will retry your db transactions up to <see cref="RetryCount"/> times for any database connection related <see cref="Exception"/>s.
	/// Working against databases such as SqlAzure that aren't always available, we need to be more resilient to connection failures by introducing retry logic.
	/// A backoff strategy is used for subsequent retries on handled <see cref="Exception"/>s, giving the database time to recover.
	/// Another option would be https://msdn.microsoft.com/en-us/data/dn456835.aspx
	/// </summary>
	public class DbConnectionRetryStrategy<T>
	{
		// Array of well known Exception Messages to be retried
		public static readonly string[] ExceptionMessagesToRetry =
		{
            // The Db isn't available from the start
            "is not currently available. Please retry the connection later",
            // While establishing the connection something went wrong
            "A network-related or instance-specific error occurred while establishing a connection to SQL Server",
            // A connection was open but something went wrong
            "A transport-level error has occurred",
            // The total amount of connections allowed by the server have been used up.
            // e.g. when scaling, existing connection aren't serviced, and new connections pile up to the point we overrun the server 
            // these should also be retried with the hope that the DB will be back soon and might catch up with the backlog of work.
            // So we might still have failures, if the DB didn't catch up soon enough.
            "The request limit for the database is",
            // Have found this one when you try and re-establish the connection
            "An existing connection was forcibly closed by the remote host",
            // When the connection is lost, it will give login failures in the first few moments after restarting the DB server
            "Login failed for user",
            // When Entity Framework tries to create the provider (DB)
            "The underlying provider failed on Open",
            // Getting only InvalidOperationException with no inner exception
            "Timeout expired",
            // "An exception has been raised that is likely due to a transient failure. If you are connecting to a SQL Azure database consider using SqlAzureExecutionStrategy"
            "An exception has been raised that is likely due to a transient failure",
            // The transaction was in doubt - so retry the whole tran
            "The transaction is in doubt"
		};

		// Array of well known Database / Connection problems to be retried
		private static readonly Type[] ExceptionTypesToHandle =
		{
			typeof(SqlException),
			typeof(InvalidOperationException),
			typeof(EntityException),
			typeof(DbUpdateException),
			typeof(TransactionInDoubtException)
		};

		/// <summary>
		/// The amount of times to retry handled <see cref="Exception"/>s
		/// </summary>
		public readonly int RetryCount;

		/// <summary>
		/// Cached <see cref="Policy"/> to reuse
		/// </summary>
		private readonly Policy _policy;
		private readonly Policy _policyAsync;

		#region Events

		/// <summary>
		/// If the <see cref="Exception"/> was handled, for every retry giving an <see cref="Exception"/> this event will fire
		/// </summary>
		public event EventHandler<ConnectionRetryExceptionEventArgs> OnRetry;

		#endregion

		public DbConnectionRetryStrategy(int retryCount)
		{
			RetryCount = retryCount;

			// cache the policy
			_policy = Policy
				// Check if the Exception should be handled
				.Handle<Exception>(ex =>
				{
					var canRetry = HasRetryableException(ex);
					return canRetry.HasValue && canRetry.Value;
				})
				// back off by an exponencial amount of time up to 30 secs max
				.WaitAndRetry(RetryCount, count => TimeSpan.FromMilliseconds(Math.Min((Math.Pow(2, count)) * 1000, 30000)),
				(exception, delay, count, context) =>
				{
					OnRetry?.Invoke(this, new ConnectionRetryExceptionEventArgs(exception, delay, count));
				});
			// cache the policy
			_policyAsync = Policy
				// Check if the Exception should be handled
				.Handle<Exception>(ex =>
				{
					var canRetry = HasRetryableException(ex);
					return canRetry.HasValue && canRetry.Value;
				})
				// back off by an exponencial amount of time up to 30 secs max
				.WaitAndRetryAsync(RetryCount, count => TimeSpan.FromMilliseconds(Math.Min((Math.Pow(2, count)) * 1000, 30000)),
					(exception, delay, count, context) =>
					{
						OnRetry?.Invoke(this, new ConnectionRetryExceptionEventArgs(exception, delay, count));
					});
		}

		/// <summary>
		/// Retries the <see cref="Func{T}"/> if it has handled <see cref="Exception"/>s or fails immediately of it has unhandled <see cref="Exception"/>s.
		/// Expects the <see cref="Func{T}"/> that will kick of a save or create a database connection.
		/// Will wrap the <see cref="Func{T}"/> in a retrying policy.
		/// <see cref="Exception"/>s caught for retrying are Sql and connection related.
		/// <see cref="OnRetry"/> will fire for each handled <see cref="Exception"/> before we reach <see cref="RetryCount"/>
		/// </summary>
		/// <param name="dbAction">The save <see cref="Func{T}"/> to execute within the policy</param>
		/// <returns>
		/// The number of state entries written to the underlying database. This can include
		/// state entries for entities and/or relationships. Relationship state entries are
		/// created for many-to-many relationships and relationships where there is no foreign
		/// key property included in the entity class (often referred to as independent associations).
		/// </returns>
		/// <exception cref="ConnectionRetryExceptionEventArgs">Thrown when either the <see cref="Exception"/> was 
		/// handled by this policy up to <see cref="RetryCount"/> or the <see cref="Exception"/> was unhandled by this policy.</exception>
		/// <seealso cref="System.Data.Entity.DbContext.SaveChanges"/>
		public T ExecuteRetrying(Func<T> dbAction)
		{
			T result = default(T);

			// After retrying up to RetryCount policyResult will contain the outcome
			var policyResult = _policy.ExecuteAndCapture<T>(dbAction);

			if (policyResult.Outcome == OutcomeType.Successful)
			{
				result = policyResult.Result;
			}
			else
			{
				if (policyResult.ExceptionType == ExceptionType.HandledByThisPolicy)
				{
					throw new DbConnectionRetryException(RetryCount, $"A retryable exception retried {RetryCount} times threw '{policyResult.FinalException.Message}'. See Inner Exception for details.", policyResult.FinalException);
				}
				else
				{
					// For unhandled exceptions, throw the original preserving its stack trace
					ExceptionDispatchInfo.Capture(policyResult.FinalException).Throw();
				}
			}

			return result;
		}

		/// <summary>
		/// Retries the <see cref="Func{T}"/> if it has handled <see cref="Exception"/>s or fails immediately of it has unhandled <see cref="Exception"/>s.
		/// Expects the <see cref="Func{T}"/> that will kick of a save or create a database connection.
		/// Will wrap the <see cref="Func{T}"/> in a retrying policy.
		/// <see cref="Exception"/>s caught for retrying are Sql and connection related.
		/// <see cref="OnRetry"/> will fire for each handled <see cref="Exception"/> before we reach <see cref="RetryCount"/>
		/// </summary>
		/// <param name="dbAction">The save <see cref="Func{T}"/> to execute within the policy</param>
		/// <returns>
		/// The number of state entries written to the underlying database. This can include
		/// state entries for entities and/or relationships. Relationship state entries are
		/// created for many-to-many relationships and relationships where there is no foreign
		/// key property included in the entity class (often referred to as independent associations).
		/// </returns>
		/// <exception cref="ConnectionRetryExceptionEventArgs">Thrown when either the <see cref="Exception"/> was 
		/// handled by this policy up to <see cref="RetryCount"/> or the <see cref="Exception"/> was unhandled by this policy.</exception>
		/// <seealso cref="System.Data.Entity.DbContext.SaveChanges"/>
		public async Task<T> ExecuteRetryingAsync(Func<Task<T>> dbAction)
		{
			T result = default(T);

			// After retrying up to RetryCount policyResult will contain the outcome
			var policyResult = await _policyAsync.ExecuteAndCaptureAsync<T>(dbAction).ConfigureAwait(false);

			if (policyResult.Outcome == OutcomeType.Successful)
			{
				result = policyResult.Result;
			}
			else
			{
				if (policyResult.ExceptionType == ExceptionType.HandledByThisPolicy)
				{
					throw new DbConnectionRetryException(RetryCount, $"A retryable exception retried {RetryCount} times threw '{policyResult.FinalException.Message}'. See Inner Exception for details.", policyResult.FinalException);
				}
				else
				{
					// For unhandled exceptions, throw the original preserving its stack trace
					ExceptionDispatchInfo.Capture(policyResult.FinalException).Throw();
				}
			}

			return result;
		}

		/// <summary>
		/// Check if the <see cref="Exception"/> should be retried
		/// </summary>
		/// <param name="exception">The <see cref="Exception"/> to retry</param>
		/// <returns>Should the <see cref="Exception"/> be retried</returns>
		private bool? HasRetryableException(Exception exception)
		{
			bool? innerExceptionRetryable = null;

			if (exception.InnerException != null)
			{
				innerExceptionRetryable = HasRetryableException(exception.InnerException);
			}

			if (innerExceptionRetryable.HasValue && innerExceptionRetryable.Value)
			{
				return innerExceptionRetryable;
			}

			if (ExceptionTypesToHandle.Contains(exception.GetType()))
			{
				return ExceptionMessagesToRetry.Any(e => exception.Message.Contains(e));
			}

			return innerExceptionRetryable;
		}
	}
}
