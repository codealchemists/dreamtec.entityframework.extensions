﻿using Dreamtec.EntityFramework.Extensions.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing
{
	public class TestVersionResolver : IVersionResolver
	{
		public string Version { get; set; }

		public string ResolveVersion(Type type)
		{
			return Version;
		}
	}
}
