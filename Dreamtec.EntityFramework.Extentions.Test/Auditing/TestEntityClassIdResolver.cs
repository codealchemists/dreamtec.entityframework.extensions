﻿using Dreamtec.EntityFramework.Extensions.Auditing;
using Dreamtec.EntityFramework.Extensions.Auditing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing
{
	public class TestEntityClassIdResolver : IEntityClassIdResolver
	{
		private IDictionary<string, int> _entityClassTypes;

		public TestEntityClassIdResolver(string dbConnection)
		{
			using (var auditLogDbContext = new AuditLogDbContext(dbConnection))
			{
				_entityClassTypes = auditLogDbContext.EntityClassTypes.ToList().ToDictionary(e => e.FullTypeName, e => e.Id);
			}
		}

		public int ResolveId(string fullEntityTypeName)
		{
			int id;
			if(_entityClassTypes.TryGetValue(fullEntityTypeName, out id))
			{
				return id;
			} else
			{
				throw new InvalidOperationException("The fullEntityTypeName [" + fullEntityTypeName  + "] could not be found");
			}
		}
	}
}
