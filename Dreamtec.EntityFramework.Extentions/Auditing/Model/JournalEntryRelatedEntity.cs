using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System;


namespace Dreamtec.EntityFramework.Extensions.Auditing.Model
{
	public class JournalEntryRelatedEntity
	{
		public virtual Guid EntityId { get; set; }

		public virtual int JournalEntryId { get; set; }
		public virtual JournalEntry JournalEntry { get; set; }

		public virtual int EntityClassTypeId { get; set; }
		public virtual EntityClassType EntityClassType { get; set; }
	}
}
