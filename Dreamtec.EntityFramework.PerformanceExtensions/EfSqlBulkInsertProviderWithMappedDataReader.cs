﻿using ResQ.Beryllium.ArgumentValidation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.PerformanceExtensions
{
	/// <summary>
	/// Class was copied and pasted from https://efbulkinsert.codeplex.com/
	/// </summary>
	public class EfSqlBulkInsertProviderWithMappedDataReader : ProviderBase<SqlConnection, SqlTransaction>
	{

		public EfSqlBulkInsertProviderWithMappedDataReader(DbContext context):base(context)
		{

		}

		/// <summary>
		/// Runs sql bulk insert using custom IDataReader
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entities"></param>
		/// <param name="transaction"></param>
		public override void Run<T>(IEnumerable<T> entities, SqlTransaction transaction)
		{
			Validate.Argument(entities, nameof(entities))
			.IsNotNull()
			.AndArgument(transaction, nameof(transaction))
			.IsNotNull();

			
			var keepIdentity = (SqlBulkCopyOptions.KeepIdentity & Options.SqlBulkCopyOptions) > 0;
			using (var reader = new MappedDataReader<T>(entities, this))
			{
				using (var sqlBulkCopy = new SqlBulkCopy(transaction.Connection, Options.SqlBulkCopyOptions, transaction))
				{
					sqlBulkCopy.BulkCopyTimeout = Options.TimeOutSeconds;
					sqlBulkCopy.BatchSize = Options.BatchSize;
					sqlBulkCopy.DestinationTableName = string.Format("[{0}].[{1}]", reader.SchemaName, reader.TableName);

					foreach (var kvp in reader.Cols)
					{
						if (kvp.Value.IsIdentity && !keepIdentity)
						{
							continue;
						}
						sqlBulkCopy.ColumnMappings.Add(kvp.Value.ColumnName, kvp.Value.ColumnName);
					}

					sqlBulkCopy.WriteToServer(reader);
				}
			}
		}

		/// <summary>
		/// Create new sql connection
		/// </summary>
		/// <returns></returns>
		protected override SqlConnection CreateConnection()
		{
			return new SqlConnection(ConnectionString);
		}
	}
}