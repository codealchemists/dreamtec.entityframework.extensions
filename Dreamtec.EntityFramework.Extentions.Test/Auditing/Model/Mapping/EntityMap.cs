using System.Data.Entity.ModelConfiguration;

namespace Dreamtec.EntityFramework.Extentions.Test.Auditing.Model.Mapping
{
    internal class EntityMap : EntityTypeConfiguration<Entity>
    {
        public EntityMap()
        {

            this.Property(t => t.Id);

            this.ToTable("Entity", "audittest");
        }
    }
}
