/* We cannot allow for table level locks when doing auditing, we need row level locking */
ALTER DATABASE AuditTest
SET ALLOW_SNAPSHOT_ISOLATION ON
GO

ALTER DATABASE AuditTest
SET READ_COMMITTED_SNAPSHOT ON
GO

CREATE SCHEMA [audit]
GO

CREATE TABLE [dbo].[EntityClassType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FullTypeName] [varchar](250) NOT NULL,
	[IsStale] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_EntityClassType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_FullTypeName] ON [dbo].[EntityClassType]
(
	[FullTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE TABLE [audit].[Transaction](
	[Id] [uniqueidentifier] NOT NULL,
	[DateApplied] [datetimeoffset](7) NOT NULL,
	[LoginId] [uniqueidentifier],
	[UserName] [nvarchar](250),
	[UserIpAddress] [varchar](45),
	[TriggerActionOwningObjectName] [varchar](250),
	[TriggerActionName] [varchar](250),
	[ExecutingMachineName] [varchar](250) NOT NULL,
	[EndpointName] [varchar](250),
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [audit].[JournalEntry](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TransactionId] [uniqueidentifier] NOT NULL,
	[DateApplied] [datetimeoffset](7) NOT NULL,
	[UserComment] [nvarchar](4000) NULL,
	[SystemComment] [varchar](8000) NULL,
	[Verbosity] [tinyint] NOT NULL,
	[Confidentiality] [tinyint] NOT NULL,
	[ActionOwningObjectName] [varchar](250) NULL,
	[ActionName] [varchar](250) NULL,
 CONSTRAINT [PK_JournalEntry_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [audit].[JournalEntryRelatedEntity](
	[JournalEntryId] [int] NOT NULL,
	[EntityClassTypeId] [int] NOT NULL,
	[EntityId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_JournalEntryRelatedEntity] PRIMARY KEY NONCLUSTERED 
(
	[JournalEntryId] ASC,
	[EntityClassTypeId] ASC,
	[EntityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [IX_EntityId] ON [audit].[JournalEntryRelatedEntity]
(
	[EntityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE TABLE [audit].[JournalEntryConfidentiality](
	[Id] [tinyint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_JournalEntryConfidentiality] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [audit].[JournalEntryVerbosity](
	[Id] [tinyint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_JournalEntryVerbosity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [audit].[Entity](
	[Id] [uniqueidentifier] NOT NULL,
	[TransactionId] [uniqueidentifier] NOT NULL,
	[AuditableEntityId] [uniqueidentifier] NOT NULL,
	[DbOperation] [char](1) NOT NULL,
	[EntityTitle] [nvarchar](250),
	[FullEntityTypeName] [varchar](250) NOT NULL,
	[Version] [varchar](250) NULL,
 CONSTRAINT [PK_Entity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [audit].[EntityProperty](
	[Id] [uniqueidentifier] NOT NULL,
	[EntityId] [uniqueidentifier] NOT NULL,
	[DataType] [varchar](100) NOT NULL,
	[PropertyName] [varchar](100) NOT NULL,
	[OriginalValue] [nvarchar](4000),
	[ModifiedValue] [nvarchar](4000),
 CONSTRAINT [PK_Property] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [audit].[Entity]  WITH CHECK ADD  CONSTRAINT [FK_Entity_Transaction] FOREIGN KEY([TransactionId])
REFERENCES [audit].[Transaction] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [audit].[Entity] CHECK CONSTRAINT [FK_Entity_Transaction]
GO

ALTER TABLE [audit].[EntityProperty]  WITH CHECK ADD  CONSTRAINT [FK_EntityProperty_Entity] FOREIGN KEY([EntityId])
REFERENCES [audit].[Entity] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [audit].[EntityProperty] CHECK CONSTRAINT [FK_EntityProperty_Entity]
GO

ALTER TABLE [audit].[JournalEntry]  WITH CHECK ADD  CONSTRAINT [FK_JournalEntry_JournalEntryConfidentiality] FOREIGN KEY([Confidentiality])
REFERENCES [audit].[JournalEntryConfidentiality] ([Id])
GO

ALTER TABLE [audit].[JournalEntry] CHECK CONSTRAINT [FK_JournalEntry_JournalEntryConfidentiality]
GO

ALTER TABLE [audit].[JournalEntry]  WITH CHECK ADD  CONSTRAINT [FK_JournalEntry_JournalEntryVerbosity] FOREIGN KEY([Verbosity])
REFERENCES [audit].[JournalEntryVerbosity] ([Id])
GO

ALTER TABLE [audit].[JournalEntry] CHECK CONSTRAINT [FK_JournalEntry_JournalEntryVerbosity]
GO

ALTER TABLE [audit].[JournalEntry]  WITH CHECK ADD  CONSTRAINT [FK_JournalEntry_Transaction] FOREIGN KEY([TransactionId])
REFERENCES [audit].[Transaction] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [audit].[JournalEntry] CHECK CONSTRAINT [FK_JournalEntry_Transaction]
GO

ALTER TABLE [audit].[JournalEntryRelatedEntity]  WITH CHECK ADD  CONSTRAINT [FK_JournalEntryRelatedEntity_EntityClassType] FOREIGN KEY([EntityClassTypeId])
REFERENCES [dbo].[EntityClassType] ([Id])
GO

ALTER TABLE [audit].[JournalEntryRelatedEntity] CHECK CONSTRAINT [FK_JournalEntryRelatedEntity_EntityClassType]
GO

ALTER TABLE [audit].[JournalEntryRelatedEntity]  WITH CHECK ADD  CONSTRAINT [FK_JournalEntryRelatedEntity_JournalEntry] FOREIGN KEY([JournalEntryId])
REFERENCES [audit].[JournalEntry] ([Id])
GO

ALTER TABLE [audit].[JournalEntryRelatedEntity] CHECK CONSTRAINT [FK_JournalEntryRelatedEntity_JournalEntry]
GO

CREATE VIEW [audit].[AuditEntryView] with schemabinding AS
SELECT Id = isnull(newid(), '00000000-0000-0000-0000-000000000000'), t.Id as TransactionId, t.DateApplied, t.UserName, t.TriggerActionOwningObjectName, t.TriggerActionName, t.EndpointName, e.DbOperation, 
e.EntityTitle, e.FullEntityTypeName, ep.DataType, ep.PropertyName, ep.OriginalValue, ep.ModifiedValue
FROM [audit].[Transaction] t
JOIN [audit].Entity e ON t.Id = e.TransactionId
JOIN [audit].EntityProperty ep ON e.Id = ep.EntityId;
GO

INSERT INTO [audit].[JournalEntryConfidentiality]([Id],[Name]) VALUES (0,'Confidential')
INSERT INTO [audit].[JournalEntryConfidentiality]([Id],[Name]) VALUES (1,'Public')

INSERT INTO [audit].[JournalEntryVerbosity]([Id],[Name]) VALUES (0,'Normal')
INSERT INTO [audit].[JournalEntryVerbosity]([Id],[Name]) VALUES (1,'Verbose')

GO