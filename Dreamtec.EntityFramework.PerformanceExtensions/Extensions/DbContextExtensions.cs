﻿using ResQ.Beryllium.ArgumentValidation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.PerformanceExtensions
{
	/// <summary>
	/// Class was copied and pasted from https://efbulkinsert.codeplex.com/
	/// </summary>
	public static class DbContextExtensions
	{
		public static void BulkInsert<T>(this DbContext context, IEnumerable<T> entities, int batchSize )
		{
			Validate.Argument(context, nameof(context))
				.IsNotNull()
				.AndArgument(entities, nameof(entities))
				.IsNotNull()
				.AndArgument(batchSize,nameof(batchSize))
				.GreaterThan(0);

			context.BulkInsert(entities, new BulkInsertOptions { BatchSize = batchSize }	);
		}

		public static void BulkInsert<T>(this DbContext context, IEnumerable<T> entities)
		{
			Validate.Argument(context, nameof(context))
				.IsNotNull()
				.AndArgument(entities, nameof(entities))
				.IsNotNull();

			context.BulkInsert(entities, new BulkInsertOptions());
		}

		public static void BulkInsert<T>(this DbContext context, IEnumerable<T> entities, BulkInsertOptions bulkInsertOptions)
		{

			Validate.Argument(context, nameof(context))
				.IsNotNull()
				.AndArgument(entities, nameof(entities))
				.IsNotNull()
				.AndArgument(bulkInsertOptions, nameof(bulkInsertOptions))
				.IsNotNull();

			var bulkInsert = new EfSqlBulkInsertProviderWithMappedDataReader(context);
			bulkInsert.Options = bulkInsertOptions;
			bulkInsert.Run(entities);
		}

	}
}
