﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
    /// <summary>
	/// Resolved an entity's <see cref="EntityClassType"/>
	/// </summary>
    public interface IEntityClassIdResolver
    {
		/// <summary>
		/// Get the <see cref="EntityClassType"/> Id for the full type name
		/// </summary>
		/// <param name="fullEntityTypeName">The full type name of the Entity</param>
		/// <returns>The <see cref="EntityClassType"/> Id for the Entity</returns>
		int ResolveId(string fullEntityTypeName);
    }
}
