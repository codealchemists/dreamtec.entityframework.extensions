﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
    /// <summary>
    /// The verbosity / level of detail of the <see cref="JournalEntry"/>
    /// </summary>
    public enum Verbosity : byte
    {
        Normal = 0,
        Verbose = 1
    }
}
