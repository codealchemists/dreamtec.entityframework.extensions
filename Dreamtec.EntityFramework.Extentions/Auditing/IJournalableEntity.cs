﻿using System;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
    /// <summary>
    /// An Entity that is journalable. 
    /// </summary>
	public interface IJournalableEntity
	{
		Guid Id { get; set; }
	}
}

