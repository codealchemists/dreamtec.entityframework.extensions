﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
	public class JournalEventArgs : EventArgs
	{
		public Model.Transaction Transaction { get; set; }
		public IList<JournalEntry> JournalEntries { get; set; }

		public JournalEventArgs(Model.Transaction transaction, IList<JournalEntry> journalEntries)
		{
			this.Transaction = transaction;
			this.JournalEntries = journalEntries;
		}
	}
}
