﻿using System;
using System.Collections.Generic;

namespace Dreamtec.EntityFramework.Extensions.Auditing
{
    /// <summary>
    /// Auditing information
    /// </summary>
	public class AuditContextState
	{
		public DateTimeOffset? DateCaptured { get; set; }
		public Guid? LoginId { get; set; }
		public string EndpointName { get; set; }
		public string TriggerActionName { get; set; }
		public string TriggerActionOwningObjectName { get; set; }
		public string UserIpAddress { get; set; }
		public string Username { get; set; }
		public IList<JournalEntry> JournalEntries { get; set; } = new List<JournalEntry>();
	}
}
