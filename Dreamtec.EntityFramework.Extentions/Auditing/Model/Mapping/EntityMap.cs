using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Dreamtec.EntityFramework.Extensions.Auditing.Model.Mapping
{
	internal class EntityMap : EntityTypeConfiguration<Entity>
	{
		public EntityMap()
		{
            this.HasKey(t => t.Id);

            this.Property(t => t.Id)
                .IsRequired();

            this.Property(t => t.TransactionId)
                .IsRequired();

            this.Property(t => t.AuditableEntityId)
                .IsRequired();

            this.Property(t => t.DbOperation)
                .IsRequired();

			this.Property(t => t.EntityTitle)
				.HasMaxLength(500);

			this.Property(t => t.FullEntityTypeName)
                .IsRequired()
                .HasMaxLength(250);

			this.Property(t => t.Version)
				.HasMaxLength(250);

			this.ToTable("Entity", "audit");

            this.HasRequired(t => t.Transaction)
				 .WithMany(t => t.Entities)
				 .HasForeignKey(d => d.TransactionId);

			// Exclude
			this.Ignore(t => t.AuditableEntity);
			this.Ignore(t => t.Entry);
        }
	}
}
