﻿using System;

namespace Dreamtec.EntityFramework.Extensions
{
    [Serializable]
    public class EntityFrameworkExtensionException : Exception
    {
        public EntityFrameworkExtensionException()
        {
        }

        public EntityFrameworkExtensionException(
            string message)
            : base(message)
        {
        }

        public EntityFrameworkExtensionException(
            string message,
            Exception innerException)
            : base(message, innerException)
        {
        }

        protected EntityFrameworkExtensionException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
    }
}
