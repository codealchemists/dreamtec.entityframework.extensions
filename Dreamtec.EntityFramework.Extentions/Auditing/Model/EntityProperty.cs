using System;

namespace Dreamtec.EntityFramework.Extensions.Auditing.Model
{
    public partial class EntityProperty
    {
		 public virtual Guid Id { get; set; }
		 public virtual Guid EntityId { get; set; }
		 public virtual string DataType { get; set; }
		 public virtual string ModifiedValue { get; set; }
		 public virtual string OriginalValue { get; set; }
		 public virtual string PropertyName { get; set; }

		 public virtual Entity Entity { get; set; }
    }
}
